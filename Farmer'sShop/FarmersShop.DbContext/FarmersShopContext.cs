﻿using FarmersShop.Domain;
using Microsoft.EntityFrameworkCore;

namespace FarmersShop.ShopDbContext
{
    public class FarmersShopContext : DbContext
    {
        public DbSet<Basket> Basket { get; set; }
        public DbSet<BasketProduct> BasketProduct { get; set; }
        public DbSet<BuyerCategory> BuyerCategories { get; set; }
        public DbSet<BuyersSubscriptions> BuyersSubscriptions { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OtherInfo> OtherInfos { get; set; }
        public DbSet<OtherInfoSubscriptions> OtherInfoSubscriptions { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Rating> Ratings { get; set; }
        public DbSet<Seller> Sellers { get; set; }
        public DbSet<ChatMessages> ChatMessages { get; set; }
        public DbSet<Administrator> Administrators { get; set; }
        public DbSet<Buyer> Buyers { get; set; }
        public DbSet<User> Users { get; set; }

        public FarmersShopContext()
        {
            Database.Migrate();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=A-104-04;Database=FarmersShop;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Ignore<UserElements>();
            modelBuilder.Entity<Seller>().ToTable("Sellers");
            modelBuilder.Entity<Administrator>().ToTable("Administrators");
            modelBuilder.Entity<Buyer>().ToTable("Buyers");
        }
    }
}
