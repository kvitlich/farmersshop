﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmersShop.ShopDbContext.Migrations
{
    public partial class LastInitConnectedWithBasket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Basket_BasketProduct_BasketProductId",
                table: "Basket");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_BasketProduct_BasketProductId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_BasketProductId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Basket_BasketProductId",
                table: "Basket");

            migrationBuilder.DropColumn(
                name: "BasketProductId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "BasketProductId",
                table: "Basket");

            migrationBuilder.AddColumn<Guid>(
                name: "BasketId",
                table: "BasketProduct",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ProductsId",
                table: "BasketProduct",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BasketProduct_BasketId",
                table: "BasketProduct",
                column: "BasketId");

            migrationBuilder.CreateIndex(
                name: "IX_BasketProduct_ProductsId",
                table: "BasketProduct",
                column: "ProductsId");

            migrationBuilder.AddForeignKey(
                name: "FK_BasketProduct_Basket_BasketId",
                table: "BasketProduct",
                column: "BasketId",
                principalTable: "Basket",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_BasketProduct_Products_ProductsId",
                table: "BasketProduct",
                column: "ProductsId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BasketProduct_Basket_BasketId",
                table: "BasketProduct");

            migrationBuilder.DropForeignKey(
                name: "FK_BasketProduct_Products_ProductsId",
                table: "BasketProduct");

            migrationBuilder.DropIndex(
                name: "IX_BasketProduct_BasketId",
                table: "BasketProduct");

            migrationBuilder.DropIndex(
                name: "IX_BasketProduct_ProductsId",
                table: "BasketProduct");

            migrationBuilder.DropColumn(
                name: "BasketId",
                table: "BasketProduct");

            migrationBuilder.DropColumn(
                name: "ProductsId",
                table: "BasketProduct");

            migrationBuilder.AddColumn<Guid>(
                name: "BasketProductId",
                table: "Products",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "BasketProductId",
                table: "Basket",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_BasketProductId",
                table: "Products",
                column: "BasketProductId");

            migrationBuilder.CreateIndex(
                name: "IX_Basket_BasketProductId",
                table: "Basket",
                column: "BasketProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Basket_BasketProduct_BasketProductId",
                table: "Basket",
                column: "BasketProductId",
                principalTable: "BasketProduct",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_BasketProduct_BasketProductId",
                table: "Products",
                column: "BasketProductId",
                principalTable: "BasketProduct",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
