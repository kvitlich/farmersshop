﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmersShop.ShopDbContext.Migrations
{
    public partial class AddedChatRelatedClasses : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BuyersSubscriptions_Buyers_BuyerId",
                table: "BuyersSubscriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Chats_OtherInfos_OtherInfoId",
                table: "Chats");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Administrators_AdministratorId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_Buyers_BuyerId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfos_Administrators_AdministratorId",
                table: "OtherInfos");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfos_OtherInfos_OtherInfoId",
                table: "OtherInfos");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfos_Buyers_PublisherBuyerId",
                table: "OtherInfos");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfos_Sellers_PublisherSellerId",
                table: "OtherInfos");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfoSubscriptions_Buyers_SubscriberBuyerId",
                table: "OtherInfoSubscriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfoSubscriptions_Sellers_SubscriberSellerId",
                table: "OtherInfoSubscriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Sellers_SellerId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_Buyers_FromBuyerId",
                table: "Ratings");

            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_Sellers_FromSellerId",
                table: "Ratings");

            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_Buyers_ToBuyerId",
                table: "Ratings");

            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_Sellers_ToSellerId",
                table: "Ratings");

            migrationBuilder.DropTable(
                name: "Administrators");

            migrationBuilder.DropTable(
                name: "Buyers");

            migrationBuilder.DropIndex(
                name: "IX_OtherInfos_OtherInfoId",
                table: "OtherInfos");

            migrationBuilder.DropIndex(
                name: "IX_Chats_OtherInfoId",
                table: "Chats");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Sellers",
                table: "Sellers");

            migrationBuilder.DropColumn(
                name: "OtherInfoId",
                table: "OtherInfos");

            migrationBuilder.DropColumn(
                name: "Message",
                table: "Chats");

            migrationBuilder.DropColumn(
                name: "OtherInfoId",
                table: "Chats");

            migrationBuilder.RenameTable(
                name: "Sellers",
                newName: "UserElements");

            migrationBuilder.AddColumn<Guid>(
                name: "AdministratorId",
                table: "Chats",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "BuyerId",
                table: "Chats",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SellerId",
                table: "Chats",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Rating",
                table: "UserElements",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddColumn<Guid>(
                name: "BuyerCategoryId",
                table: "UserElements",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Seller_Rating",
                table: "UserElements",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "UserElements",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddPrimaryKey(
                name: "PK_UserElements",
                table: "UserElements",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "ChatMessages",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    Text = table.Column<string>(nullable: true),
                    OtherInfoId = table.Column<Guid>(nullable: true),
                    FromUserId = table.Column<Guid>(nullable: true),
                    ChatId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ChatMessages", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ChatMessages_Chats_ChatId",
                        column: x => x.ChatId,
                        principalTable: "Chats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChatMessages_UserElements_FromUserId",
                        column: x => x.FromUserId,
                        principalTable: "UserElements",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ChatMessages_OtherInfos_OtherInfoId",
                        column: x => x.OtherInfoId,
                        principalTable: "OtherInfos",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Chats_AdministratorId",
                table: "Chats",
                column: "AdministratorId");

            migrationBuilder.CreateIndex(
                name: "IX_Chats_BuyerId",
                table: "Chats",
                column: "BuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_Chats_SellerId",
                table: "Chats",
                column: "SellerId");

            migrationBuilder.CreateIndex(
                name: "IX_UserElements_BuyerCategoryId",
                table: "UserElements",
                column: "BuyerCategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessages_ChatId",
                table: "ChatMessages",
                column: "ChatId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessages_FromUserId",
                table: "ChatMessages",
                column: "FromUserId");

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessages_OtherInfoId",
                table: "ChatMessages",
                column: "OtherInfoId");

            migrationBuilder.AddForeignKey(
                name: "FK_BuyersSubscriptions_UserElements_BuyerId",
                table: "BuyersSubscriptions",
                column: "BuyerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Chats_UserElements_AdministratorId",
                table: "Chats",
                column: "AdministratorId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Chats_UserElements_BuyerId",
                table: "Chats",
                column: "BuyerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Chats_UserElements_SellerId",
                table: "Chats",
                column: "SellerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_UserElements_AdministratorId",
                table: "Orders",
                column: "AdministratorId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_UserElements_BuyerId",
                table: "Orders",
                column: "BuyerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfos_UserElements_AdministratorId",
                table: "OtherInfos",
                column: "AdministratorId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfos_UserElements_PublisherBuyerId",
                table: "OtherInfos",
                column: "PublisherBuyerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfos_UserElements_PublisherSellerId",
                table: "OtherInfos",
                column: "PublisherSellerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfoSubscriptions_UserElements_SubscriberBuyerId",
                table: "OtherInfoSubscriptions",
                column: "SubscriberBuyerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfoSubscriptions_UserElements_SubscriberSellerId",
                table: "OtherInfoSubscriptions",
                column: "SubscriberSellerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_UserElements_SellerId",
                table: "Products",
                column: "SellerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_UserElements_FromBuyerId",
                table: "Ratings",
                column: "FromBuyerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_UserElements_FromSellerId",
                table: "Ratings",
                column: "FromSellerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_UserElements_ToBuyerId",
                table: "Ratings",
                column: "ToBuyerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_UserElements_ToSellerId",
                table: "Ratings",
                column: "ToSellerId",
                principalTable: "UserElements",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_UserElements_BuyerCategories_BuyerCategoryId",
                table: "UserElements",
                column: "BuyerCategoryId",
                principalTable: "BuyerCategories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BuyersSubscriptions_UserElements_BuyerId",
                table: "BuyersSubscriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Chats_UserElements_AdministratorId",
                table: "Chats");

            migrationBuilder.DropForeignKey(
                name: "FK_Chats_UserElements_BuyerId",
                table: "Chats");

            migrationBuilder.DropForeignKey(
                name: "FK_Chats_UserElements_SellerId",
                table: "Chats");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_UserElements_AdministratorId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_Orders_UserElements_BuyerId",
                table: "Orders");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfos_UserElements_AdministratorId",
                table: "OtherInfos");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfos_UserElements_PublisherBuyerId",
                table: "OtherInfos");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfos_UserElements_PublisherSellerId",
                table: "OtherInfos");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfoSubscriptions_UserElements_SubscriberBuyerId",
                table: "OtherInfoSubscriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_OtherInfoSubscriptions_UserElements_SubscriberSellerId",
                table: "OtherInfoSubscriptions");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_UserElements_SellerId",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_UserElements_FromBuyerId",
                table: "Ratings");

            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_UserElements_FromSellerId",
                table: "Ratings");

            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_UserElements_ToBuyerId",
                table: "Ratings");

            migrationBuilder.DropForeignKey(
                name: "FK_Ratings_UserElements_ToSellerId",
                table: "Ratings");

            migrationBuilder.DropForeignKey(
                name: "FK_UserElements_BuyerCategories_BuyerCategoryId",
                table: "UserElements");

            migrationBuilder.DropTable(
                name: "ChatMessages");

            migrationBuilder.DropIndex(
                name: "IX_Chats_AdministratorId",
                table: "Chats");

            migrationBuilder.DropIndex(
                name: "IX_Chats_BuyerId",
                table: "Chats");

            migrationBuilder.DropIndex(
                name: "IX_Chats_SellerId",
                table: "Chats");

            migrationBuilder.DropPrimaryKey(
                name: "PK_UserElements",
                table: "UserElements");

            migrationBuilder.DropIndex(
                name: "IX_UserElements_BuyerCategoryId",
                table: "UserElements");

            migrationBuilder.DropColumn(
                name: "AdministratorId",
                table: "Chats");

            migrationBuilder.DropColumn(
                name: "BuyerId",
                table: "Chats");

            migrationBuilder.DropColumn(
                name: "SellerId",
                table: "Chats");

            migrationBuilder.DropColumn(
                name: "BuyerCategoryId",
                table: "UserElements");

            migrationBuilder.DropColumn(
                name: "Seller_Rating",
                table: "UserElements");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "UserElements");

            migrationBuilder.RenameTable(
                name: "UserElements",
                newName: "Sellers");

            migrationBuilder.AddColumn<Guid>(
                name: "OtherInfoId",
                table: "OtherInfos",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Message",
                table: "Chats",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "OtherInfoId",
                table: "Chats",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "Rating",
                table: "Sellers",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_Sellers",
                table: "Sellers",
                column: "Id");

            migrationBuilder.CreateTable(
                name: "Administrators",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Adress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CretionDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Nickname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Administrators", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Buyers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Adress = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    BuyerCategoryId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    CretionDate = table.Column<DateTime>(type: "datetime2", nullable: false),
                    DeletedDate = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Nickname = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Rating = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Buyers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Buyers_BuyerCategories_BuyerCategoryId",
                        column: x => x.BuyerCategoryId,
                        principalTable: "BuyerCategories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_OtherInfos_OtherInfoId",
                table: "OtherInfos",
                column: "OtherInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Chats_OtherInfoId",
                table: "Chats",
                column: "OtherInfoId");

            migrationBuilder.CreateIndex(
                name: "IX_Buyers_BuyerCategoryId",
                table: "Buyers",
                column: "BuyerCategoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_BuyersSubscriptions_Buyers_BuyerId",
                table: "BuyersSubscriptions",
                column: "BuyerId",
                principalTable: "Buyers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Chats_OtherInfos_OtherInfoId",
                table: "Chats",
                column: "OtherInfoId",
                principalTable: "OtherInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Administrators_AdministratorId",
                table: "Orders",
                column: "AdministratorId",
                principalTable: "Administrators",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Orders_Buyers_BuyerId",
                table: "Orders",
                column: "BuyerId",
                principalTable: "Buyers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfos_Administrators_AdministratorId",
                table: "OtherInfos",
                column: "AdministratorId",
                principalTable: "Administrators",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfos_OtherInfos_OtherInfoId",
                table: "OtherInfos",
                column: "OtherInfoId",
                principalTable: "OtherInfos",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfos_Buyers_PublisherBuyerId",
                table: "OtherInfos",
                column: "PublisherBuyerId",
                principalTable: "Buyers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfos_Sellers_PublisherSellerId",
                table: "OtherInfos",
                column: "PublisherSellerId",
                principalTable: "Sellers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfoSubscriptions_Buyers_SubscriberBuyerId",
                table: "OtherInfoSubscriptions",
                column: "SubscriberBuyerId",
                principalTable: "Buyers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_OtherInfoSubscriptions_Sellers_SubscriberSellerId",
                table: "OtherInfoSubscriptions",
                column: "SubscriberSellerId",
                principalTable: "Sellers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Sellers_SellerId",
                table: "Products",
                column: "SellerId",
                principalTable: "Sellers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_Buyers_FromBuyerId",
                table: "Ratings",
                column: "FromBuyerId",
                principalTable: "Buyers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_Sellers_FromSellerId",
                table: "Ratings",
                column: "FromSellerId",
                principalTable: "Sellers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_Buyers_ToBuyerId",
                table: "Ratings",
                column: "ToBuyerId",
                principalTable: "Buyers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Ratings_Sellers_ToSellerId",
                table: "Ratings",
                column: "ToSellerId",
                principalTable: "Sellers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
