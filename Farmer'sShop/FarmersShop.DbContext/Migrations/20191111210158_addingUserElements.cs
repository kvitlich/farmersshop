﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmersShop.ShopDbContext.Migrations
{
    public partial class addingUserElements : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FullName",
                table: "Administrators");

            migrationBuilder.AddColumn<string>(
                name: "Adress",
                table: "Administrators",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nickname",
                table: "Administrators",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Adress",
                table: "Administrators");

            migrationBuilder.DropColumn(
                name: "Nickname",
                table: "Administrators");

            migrationBuilder.AddColumn<string>(
                name: "FullName",
                table: "Administrators",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
