﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmersShop.ShopDbContext.Migrations
{
    public partial class OrderAdding : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BasketProduct_Products_ProductsId",
                table: "BasketProduct");

            migrationBuilder.DropIndex(
                name: "IX_BasketProduct_ProductsId",
                table: "BasketProduct");

            migrationBuilder.DropColumn(
                name: "ProductsId",
                table: "BasketProduct");

            migrationBuilder.AddColumn<DateTime>(
                name: "CheckedDate",
                table: "Orders",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<Guid>(
                name: "ProductId",
                table: "BasketProduct",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BasketProduct_ProductId",
                table: "BasketProduct",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_BasketProduct_Products_ProductId",
                table: "BasketProduct",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BasketProduct_Products_ProductId",
                table: "BasketProduct");

            migrationBuilder.DropIndex(
                name: "IX_BasketProduct_ProductId",
                table: "BasketProduct");

            migrationBuilder.DropColumn(
                name: "CheckedDate",
                table: "Orders");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "BasketProduct");

            migrationBuilder.AddColumn<Guid>(
                name: "ProductsId",
                table: "BasketProduct",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BasketProduct_ProductsId",
                table: "BasketProduct",
                column: "ProductsId");

            migrationBuilder.AddForeignKey(
                name: "FK_BasketProduct_Products_ProductsId",
                table: "BasketProduct",
                column: "ProductsId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
