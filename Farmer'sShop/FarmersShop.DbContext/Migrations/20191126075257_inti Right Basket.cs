﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmersShop.ShopDbContext.Migrations
{
    public partial class intiRightBasket : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BasketProduct_Basket_BasketId",
                table: "BasketProduct");

            migrationBuilder.DropIndex(
                name: "IX_BasketProduct_BasketId",
                table: "BasketProduct");

            migrationBuilder.DropColumn(
                name: "BasketId",
                table: "BasketProduct");

            migrationBuilder.AddColumn<Guid>(
                name: "BasketProductId",
                table: "Basket",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Basket_BasketProductId",
                table: "Basket",
                column: "BasketProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Basket_BasketProduct_BasketProductId",
                table: "Basket",
                column: "BasketProductId",
                principalTable: "BasketProduct",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Basket_BasketProduct_BasketProductId",
                table: "Basket");

            migrationBuilder.DropIndex(
                name: "IX_Basket_BasketProductId",
                table: "Basket");

            migrationBuilder.DropColumn(
                name: "BasketProductId",
                table: "Basket");

            migrationBuilder.AddColumn<Guid>(
                name: "BasketId",
                table: "BasketProduct",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BasketProduct_BasketId",
                table: "BasketProduct",
                column: "BasketId");

            migrationBuilder.AddForeignKey(
                name: "FK_BasketProduct_Basket_BasketId",
                table: "BasketProduct",
                column: "BasketId",
                principalTable: "Basket",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
