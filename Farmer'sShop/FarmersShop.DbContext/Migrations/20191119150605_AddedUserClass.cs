﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmersShop.ShopDbContext.Migrations
{
    public partial class AddedUserClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropColumn(
            //    name: "Discriminator",
            //    table: "Sellers");

            //migrationBuilder.DropColumn(
            //    name: "Discriminator",
            //    table: "Buyers");

            //migrationBuilder.DropColumn(
            //    name: "Discriminator",
            //    table: "Administrators");

            migrationBuilder.AddColumn<Guid>(
                name: "FromUserId",
                table: "ChatMessages",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    CretionDate = table.Column<DateTime>(nullable: false),
                    DeletedDate = table.Column<DateTime>(nullable: true),
                    EntityId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ChatMessages_FromUserId",
                table: "ChatMessages",
                column: "FromUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_ChatMessages_Users_FromUserId",
                table: "ChatMessages",
                column: "FromUserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ChatMessages_Users_FromUserId",
                table: "ChatMessages");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropIndex(
                name: "IX_ChatMessages_FromUserId",
                table: "ChatMessages");

            migrationBuilder.DropColumn(
                name: "FromUserId",
                table: "ChatMessages");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Sellers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Buyers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Administrators",
                nullable: false,
                defaultValue: "");
        }
    }
}
