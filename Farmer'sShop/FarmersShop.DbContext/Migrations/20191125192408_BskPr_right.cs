﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmersShop.ShopDbContext.Migrations
{
    public partial class BskPr_right : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BasketProduct_Products_ProductId",
                table: "BasketProduct");

            migrationBuilder.DropIndex(
                name: "IX_BasketProduct_ProductId",
                table: "BasketProduct");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "BasketProduct");

            migrationBuilder.AddColumn<Guid>(
                name: "BasketProductId",
                table: "Products",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Products_BasketProductId",
                table: "Products",
                column: "BasketProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_BasketProduct_BasketProductId",
                table: "Products",
                column: "BasketProductId",
                principalTable: "BasketProduct",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_BasketProduct_BasketProductId",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_BasketProductId",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "BasketProductId",
                table: "Products");

            migrationBuilder.AddColumn<Guid>(
                name: "ProductId",
                table: "BasketProduct",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_BasketProduct_ProductId",
                table: "BasketProduct",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_BasketProduct_Products_ProductId",
                table: "BasketProduct",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
