﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace FarmersShop.ShopDbContext.Migrations
{
    public partial class CorrectedRelationClass : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Relations_Administrators_FirstAdministratorId",
                table: "Relations");

            migrationBuilder.DropForeignKey(
                name: "FK_Relations_Buyers_FirstBuyerId",
                table: "Relations");

            migrationBuilder.DropForeignKey(
                name: "FK_Relations_Sellers_FirstSellerId",
                table: "Relations");

            migrationBuilder.DropForeignKey(
                name: "FK_Relations_Administrators_SecondAdministratorId",
                table: "Relations");

            migrationBuilder.DropForeignKey(
                name: "FK_Relations_Buyers_SecondBuyerId",
                table: "Relations");

            migrationBuilder.DropForeignKey(
                name: "FK_Relations_Sellers_SecondSellerId",
                table: "Relations");

            migrationBuilder.DropIndex(
                name: "IX_Relations_FirstAdministratorId",
                table: "Relations");

            migrationBuilder.DropIndex(
                name: "IX_Relations_FirstBuyerId",
                table: "Relations");

            migrationBuilder.DropIndex(
                name: "IX_Relations_FirstSellerId",
                table: "Relations");

            migrationBuilder.DropIndex(
                name: "IX_Relations_SecondAdministratorId",
                table: "Relations");

            migrationBuilder.DropIndex(
                name: "IX_Relations_SecondBuyerId",
                table: "Relations");

            migrationBuilder.DropIndex(
                name: "IX_Relations_SecondSellerId",
                table: "Relations");

            migrationBuilder.DropColumn(
                name: "FirstAdministratorId",
                table: "Relations");

            migrationBuilder.DropColumn(
                name: "FirstBuyerId",
                table: "Relations");

            migrationBuilder.DropColumn(
                name: "FirstSellerId",
                table: "Relations");

            migrationBuilder.DropColumn(
                name: "SecondAdministratorId",
                table: "Relations");

            migrationBuilder.DropColumn(
                name: "SecondBuyerId",
                table: "Relations");

            migrationBuilder.DropColumn(
                name: "SecondSellerId",
                table: "Relations");

            migrationBuilder.AddColumn<Guid>(
                name: "AdministratorId",
                table: "Relations",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "BuyerId",
                table: "Relations",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SellerId",
                table: "Relations",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Relations_AdministratorId",
                table: "Relations",
                column: "AdministratorId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_BuyerId",
                table: "Relations",
                column: "BuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_SellerId",
                table: "Relations",
                column: "SellerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Relations_Administrators_AdministratorId",
                table: "Relations",
                column: "AdministratorId",
                principalTable: "Administrators",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Relations_Buyers_BuyerId",
                table: "Relations",
                column: "BuyerId",
                principalTable: "Buyers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Relations_Sellers_SellerId",
                table: "Relations",
                column: "SellerId",
                principalTable: "Sellers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Relations_Administrators_AdministratorId",
                table: "Relations");

            migrationBuilder.DropForeignKey(
                name: "FK_Relations_Buyers_BuyerId",
                table: "Relations");

            migrationBuilder.DropForeignKey(
                name: "FK_Relations_Sellers_SellerId",
                table: "Relations");

            migrationBuilder.DropIndex(
                name: "IX_Relations_AdministratorId",
                table: "Relations");

            migrationBuilder.DropIndex(
                name: "IX_Relations_BuyerId",
                table: "Relations");

            migrationBuilder.DropIndex(
                name: "IX_Relations_SellerId",
                table: "Relations");

            migrationBuilder.DropColumn(
                name: "AdministratorId",
                table: "Relations");

            migrationBuilder.DropColumn(
                name: "BuyerId",
                table: "Relations");

            migrationBuilder.DropColumn(
                name: "SellerId",
                table: "Relations");

            migrationBuilder.AddColumn<Guid>(
                name: "FirstAdministratorId",
                table: "Relations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "FirstBuyerId",
                table: "Relations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "FirstSellerId",
                table: "Relations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SecondAdministratorId",
                table: "Relations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SecondBuyerId",
                table: "Relations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "SecondSellerId",
                table: "Relations",
                type: "uniqueidentifier",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Relations_FirstAdministratorId",
                table: "Relations",
                column: "FirstAdministratorId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_FirstBuyerId",
                table: "Relations",
                column: "FirstBuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_FirstSellerId",
                table: "Relations",
                column: "FirstSellerId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_SecondAdministratorId",
                table: "Relations",
                column: "SecondAdministratorId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_SecondBuyerId",
                table: "Relations",
                column: "SecondBuyerId");

            migrationBuilder.CreateIndex(
                name: "IX_Relations_SecondSellerId",
                table: "Relations",
                column: "SecondSellerId");

            migrationBuilder.AddForeignKey(
                name: "FK_Relations_Administrators_FirstAdministratorId",
                table: "Relations",
                column: "FirstAdministratorId",
                principalTable: "Administrators",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Relations_Buyers_FirstBuyerId",
                table: "Relations",
                column: "FirstBuyerId",
                principalTable: "Buyers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Relations_Sellers_FirstSellerId",
                table: "Relations",
                column: "FirstSellerId",
                principalTable: "Sellers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Relations_Administrators_SecondAdministratorId",
                table: "Relations",
                column: "SecondAdministratorId",
                principalTable: "Administrators",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Relations_Buyers_SecondBuyerId",
                table: "Relations",
                column: "SecondBuyerId",
                principalTable: "Buyers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Relations_Sellers_SecondSellerId",
                table: "Relations",
                column: "SecondSellerId",
                principalTable: "Sellers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
