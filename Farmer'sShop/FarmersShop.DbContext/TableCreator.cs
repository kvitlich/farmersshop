﻿using FarmersShop.Domain;

namespace FarmersShop.ShopDbContext
{
    public class TablesCreator
    {
        public static void Create()
        {
            using (var context = new FarmersShopContext())
            {
                context.SaveChanges();
            }
        }

        public static void Fill()
        {
            using (var context = new FarmersShopContext())
            {
                Administrator firstAdmin = new Administrator
                {
                    Nickname = "Admin",
                    Email = "admin@gmail.com",
                    Password = "adminPassword",
                    Adress = "Admin's Adress"
                };

                BuyerCategory firstBuyerCategory = new BuyerCategory { BuyerCategoryName = "Физ. лицо" };
                BuyerCategory secondBuyerCategory = new BuyerCategory { BuyerCategoryName = "Ресторан" };

                Buyer firstBuyer = new Buyer
                {
                    Nickname = "First Buyer",
                    Email = "firstbuyer@gmail.com",
                    Password = "firstBuyerPassword",
                    Adress = "First Buyer's Adress",
                    Rating = 3,
                    BuyerCategory = firstBuyerCategory
                };
                Buyer secondBuyer = new Buyer
                {
                    Nickname = "Second Buyer",
                    Email = "secondbuyer@gmail.com",
                    Password = "secondBuyerPassword",
                    Adress = "Second Buyer's Adress",
                    Rating = 4,
                    BuyerCategory = secondBuyerCategory
                };

                Seller firstSeller = new Seller
                {
                    Nickname = "First Seller",
                    Email = "firstseller@gmail.com",
                    Password = "firstSellerPassword",
                    Adress = "First Seller's Adress",
                    Rating = 3
                };
                Seller secondSeller = new Seller
                {
                    Nickname = "Second Seller",
                    Email = "secondseller@gmail.com",
                    Password = "secondSellerPassword",
                    Adress = "Second Seller's Adress",
                    Rating = 3
                };

                User firstUser = new User
                {
                    Id = firstAdmin.Id,
                    Type = 1,
                    Nickname = firstAdmin.Nickname
                };
                User secondUser = new User
                {
                    Id = firstBuyer.Id,
                    Type = 3,
                    Nickname = firstBuyer.Nickname
                };
                User thirdUser = new User
                {
                    Id = secondBuyer.Id,
                    Type = 3,
                    Nickname = secondBuyer.Nickname
                };
                User fourthUser = new User
                {
                    Id = firstSeller.Id,
                    Type = 2,
                    Nickname = firstSeller.Nickname
                };
                User fifthUser = new User
                {
                    Id = secondSeller.Id,
                    Type = 2,
                    Nickname = secondSeller.Nickname
                };

                ProductCategory firstProductCategory = new ProductCategory { ProductCategoryName = "Фрукты" };
                ProductCategory secondProductCategory = new ProductCategory { ProductCategoryName = "Овощи" };
                ProductCategory thirdProductCategory = new ProductCategory { ProductCategoryName = "Ягоды" };

                Product firstProduct = new Product
                {
                    ProductName = "Зелёные яблоки",
                    Description = "Красывый спэлый зиленый яблака!!!! Налитай!!!!",
                    Image = "https://cdn.shopify.com/s/files/1/0923/5524/products/Green-Apple-Balsamic-Vinegar-White_600x.jpg?v=1474991096",
                    Seller = firstSeller,
                    ProductCategory = firstProductCategory,
                    PricePerKg = 160,
                    Kg = 20,
                    Discount = 0
                };
                Product secondProduct = new Product
                {
                    ProductName = "Картошка",
                    Description = "Картошка сытная! В Беларуси такой не сыщешь!",
                    Image = "https://www.thespruceeats.com/thmb/BP-azlYCBhWus--p2AHLKAQDG7k=/2224x1668/smart/filters:no_upscale()/new-potatoes-basket-25-56a8c4743df78cf772a070df.jpg",
                    Seller = secondSeller,
                    ProductCategory = secondProductCategory,
                    PricePerKg = 200,
                    Kg = 60,
                    Discount = 10
                };

                OtherInfo firstOtherInfo = new OtherInfo
                {
                    Title = "Рецепт жареной картошки",
                    Description = "1. Нарезать картошку; 2. Пожарить картошку.",
                    PublisherSeller = secondSeller,
                    Administrator = firstAdmin,
                    IsAcceptable = true
                };

                OtherInfoSubscriptions firstOtherInfoSubscription = new OtherInfoSubscriptions
                {
                    OtherInfo = firstOtherInfo,
                    SubscriberBuyer = secondBuyer
                };
                OtherInfoSubscriptions secondOtherInfoSubscription = new OtherInfoSubscriptions
                {
                    OtherInfo = firstOtherInfo,
                    SubscriberSeller = firstSeller
                };

                Chat firstChat = new Chat
                {
                    Administrator = firstAdmin,
                    Buyer = firstBuyer,
                    Seller = secondSeller
                };
                Chat secondChat = new Chat
                {
                    Administrator = firstAdmin,
                    Buyer = secondBuyer,
                    Seller = secondSeller
                };

                ChatMessages firstChatMessage = new ChatMessages
                {
                    Text = "Здравствуйте!",
                    Chat = firstChat,
                    FromUser = fifthUser
                };
                ChatMessages secondChatMessage = new ChatMessages
                {
                    Text = "Здаров",
                    Chat = firstChat,
                    FromUser = secondUser
                };
                ChatMessages thirdChatMessage = new ChatMessages
                {
                    Text = "Как я могу купить Ваш продукт?",
                    Chat = secondChat,
                    FromUser = thirdUser
                };

                BuyersSubscriptions firstBuyerSubscription = new BuyersSubscriptions
                {
                    Buyer = firstBuyer,
                    ProductCategory = firstProductCategory
                };
                BuyersSubscriptions secondBuyerSubscription = new BuyersSubscriptions
                {
                    Buyer = secondBuyer,
                    ProductCategory = secondProductCategory
                };

                Basket firstBasket = new Basket { Buyer = firstBuyer };

                //BasketProduct firstBasketProduct = new BasketProduct
                //{ 
                //    Basket = firstBasket,
                //    Product = secondProduct
                //};

                Order firstOrder = new Order
                {
                    Buyer = secondBuyer,
                    Product = firstProduct,
                    Kgs = 2,
                    Administrator = firstAdmin
                };
                Order secondOrder = new Order
                {
                    Buyer = secondBuyer,
                    Product = firstProduct,
                    Kgs = 40,
                    Administrator = firstAdmin
                };

                Rating firstRating = new Rating
                {
                    FromSeller = firstSeller,
                    ToBuyer = secondBuyer,
                    Comment = "First comment to the First seller from the Second seller",
                    Grade = 3
                };

                context.Administrators.Add(firstAdmin);

                context.BuyerCategories.Add(firstBuyerCategory);
                context.BuyerCategories.Add(secondBuyerCategory);

                context.Buyers.Add(firstBuyer);
                context.Buyers.Add(secondBuyer);

                context.Sellers.Add(firstSeller);
                context.Sellers.Add(secondSeller);

                context.Users.Add(firstUser);
                context.Users.Add(secondUser);
                context.Users.Add(thirdUser);
                context.Users.Add(fourthUser);
                context.Users.Add(fifthUser);

                context.ProductCategories.Add(firstProductCategory);
                context.ProductCategories.Add(secondProductCategory);
                context.ProductCategories.Add(thirdProductCategory);

                context.Products.Add(firstProduct);
                context.Products.Add(secondProduct);

                context.OtherInfos.Add(firstOtherInfo);

                context.OtherInfoSubscriptions.Add(firstOtherInfoSubscription);
                context.OtherInfoSubscriptions.Add(secondOtherInfoSubscription);

                context.Chats.Add(firstChat);
                context.Chats.Add(secondChat);

                context.ChatMessages.Add(firstChatMessage);
                context.ChatMessages.Add(secondChatMessage);
                context.ChatMessages.Add(thirdChatMessage);

                context.BuyersSubscriptions.Add(firstBuyerSubscription);
                context.BuyersSubscriptions.Add(secondBuyerSubscription);

                context.Basket.Add(firstBasket);

                //     context.BasketProduct.Add(firstBasketProduct);

                context.Orders.Add(secondOrder);
                context.Orders.Add(firstOrder);

                context.Ratings.Add(firstRating);

                context.SaveChanges();
            }
        }
    }
}
