﻿using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using UI.Registration;
using UI.SignIn;

namespace UI
{
    public partial class StarterPage : Window
    {
        public StarterPage()
        {
            InitializeComponent();
            //TablesCreator.Fill();
        }

        private void Registration(object sender, RoutedEventArgs e)
        {
            UserTypeChoice userTypeChoice = new UserTypeChoice();
            userTypeChoice.Show();
            this.Close();
        }

        private void SignIn(object sender, RoutedEventArgs e)
        {
            SignInUserType signInUserType = new SignInUserType();
            signInUserType.Show();
            this.Close();

        }
    }
}
