﻿using FarmersShop.ShopDbContext;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace UI.SignIn
{
    public partial class SignInEmail : Page
    {
        private bool emailExists = false;

        public SignInEmail()
        {
            InitializeComponent();
        }

        private void NextClick(object sender, RoutedEventArgs e)
        {
            if (emailUser.Text != String.Empty)
            {
                emailExists = false;

                if (SignInUserType.account.GetUserType() == 1)
                {
                    using (var context = new FarmersShopContext())
                    {
                        var allAdmins = context.Administrators.ToList();
                        foreach (var admin in allAdmins)
                        {
                            if (admin.Email == emailUser.Text)
                            {
                                emailExists = true;
                                break;
                            }
                        }
                    }
                }
                else if (SignInUserType.account.GetUserType() == 2)
                {
                    using (var context = new FarmersShopContext())
                    {
                        var allSellers = context.Sellers.ToList();
                        foreach (var seller in allSellers)
                        {
                            if (seller.Email == emailUser.Text)
                            {
                                emailExists = true;
                                break;
                            }
                        }
                    }
                }
                else if (SignInUserType.account.GetUserType() == 3)
                {
                    using (var context = new FarmersShopContext())
                    {
                        var allBuyers = context.Buyers.ToList();
                        foreach (var buyer in allBuyers)
                        {
                            if (buyer.Email == emailUser.Text)
                            {
                                emailExists = true;
                                break;
                            }
                        }
                    }
                }

                if (emailExists == true)
                {
                    SignInUserType.account.SetUserEmail(emailUser.Text);
                    this.NavigationService.Navigate(new Uri("pack://application:,,,/SignIn/SignInPassword.xaml"));
                }
                else
                {
                    errorMessage.Text = "Ошибка! Повторите ещё раз.";
                }
            }
            else
            {
                errorMessage.Text = "Ошибка! Повторите ещё раз.";
            }
        }
    }
}
