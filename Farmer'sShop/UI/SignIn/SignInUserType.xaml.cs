﻿using FarmersShop.Services;
using System;
using System.Windows;

namespace UI.SignIn
{
    public partial class SignInUserType : Window
    {
        public static Account account;

        public SignInUserType()
        {
            InitializeComponent();
        }

        private void NextClick(object sender, RoutedEventArgs e)
        {
            account = new Account();
            account.SetUserType(typeChoise.SelectedIndex + 1);

            nextButton.Visibility = Visibility.Hidden;
            typeChoise.Visibility = Visibility.Hidden;
            typeTitle.Visibility = Visibility.Hidden;

            frame.NavigationService.Navigate(new Uri("pack://application:,,,/SignIn/SignInEmail.xaml"));
        }
    }
}
