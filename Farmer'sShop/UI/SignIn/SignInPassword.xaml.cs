﻿using FarmersShop.ShopDbContext;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace UI.SignIn
{
    public partial class SignInPassword : Page
    {
        private bool correctPassword = false;

        public SignInPassword()
        {
            InitializeComponent();
        }

        private void EndButtonClick(object sender, RoutedEventArgs e)
        {
            if (passwordUser.Password != String.Empty)
            {
                correctPassword = false;
                string email = SignInUserType.account.GetUserEmail();

                if (SignInUserType.account.GetUserType() == 1)
                {
                    using (var context = new FarmersShopContext())
                    {
                        var adminPassowd = context.Administrators.Where(a => a.Email.Equals(email));
                        if (adminPassowd.FirstOrDefault().Password == passwordUser.Password)
                        {
                            SignInUserType.account.SetUserNicknameAndEntityId(adminPassowd.FirstOrDefault().Nickname);
                            correctPassword = true;
                        }
                    }
                }
                else if (SignInUserType.account.GetUserType() == 2)
                {
                    using (var context = new FarmersShopContext())
                    {
                        var sellerPassowd = context.Sellers.Where(s => s.Email.Equals(email));
                        if (sellerPassowd.FirstOrDefault().Password == passwordUser.Password)
                        {
                            SignInUserType.account.SetUserNicknameAndEntityId(sellerPassowd.FirstOrDefault().Nickname);
                            correctPassword = true;
                        }
                    }
                }
                else if (SignInUserType.account.GetUserType() == 3)
                {
                    using (var context = new FarmersShopContext())
                    {
                        var buyerPassowd = context.Buyers.Where(b => b.Email.Equals(email));
                        if (buyerPassowd.FirstOrDefault().Password == passwordUser.Password)
                        {
                            SignInUserType.account.SetUserNicknameAndEntityId(buyerPassowd.FirstOrDefault().Nickname);
                            correctPassword = true;
                        }
                    }
                }

                if (correctPassword == true)
                {
                    if (SignInUserType.account.GetUserType() == 1)
                    {
                        StarterPage starterPage=new StarterPage();
                        starterPage.Show();
                        Application.Current.Windows[0].Close();
                    }
                    if (SignInUserType.account.GetUserType() == 2)
                    {
                        Seller.SellerWindow sellerWindow = new Seller.SellerWindow();
                        sellerWindow.Show();
                        Application.Current.Windows[0].Close();
                    }
                    if (SignInUserType.account.GetUserType() == 3)
                    {
                        Buyer.BuyerWindow buyerWindow = new Buyer.BuyerWindow();
                        buyerWindow.Show();
                        Application.Current.Windows[0].Close();
                    }
                }
                else
                {
                    errorMessage.Text = "Ошибка! Повторите ещё раз.";
                }
            }
            else
            {
                errorMessage.Text = "Ошибка! Повторите ещё раз.";
            }
        }
    }
}
