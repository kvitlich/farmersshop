﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace UI.Registration
{
    public partial class Nickname : Page
    {
        public Nickname()
        {
            InitializeComponent();
        }

        private void NextClick(object sender, RoutedEventArgs e)
        {
            if (nicknameUser.Text != String.Empty)
            {
                UserTypeChoice.registrationService.SetUserNickname(nicknameUser.Text);
                this.NavigationService.Navigate(new Uri("pack://application:,,,/Registration/Address.xaml"));
            }
            else
            {
                MessageBox.Show("Ошибка! Повторите ещё раз.");
            }
        }
    }
}
