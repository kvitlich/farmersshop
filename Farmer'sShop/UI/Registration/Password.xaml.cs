﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace UI.Registration
{
    public partial class Password : Page
    {
        public Password()
        {
            InitializeComponent();
        }

        private void NextClick(object sender, RoutedEventArgs e)
        {
            if(passwordUser.Text != String.Empty)
            {
                UserTypeChoice.registrationService.SetUserPassword(passwordUser.Text);

            this.NavigationService.Navigate(new Uri("pack://application:,,,/Registration/Nickname.xaml"));
            }
            else
            {
                MessageBox.Show("Ошибка! Повторите ещё раз.");
            }
        }
    }
}
