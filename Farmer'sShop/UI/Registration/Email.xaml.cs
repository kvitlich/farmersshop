﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace UI.Registration
{
    public partial class Email : Page
    {
        public Email()
        {
            InitializeComponent();
        }


        private void NextButtonClick(object sender, RoutedEventArgs e)
        {
            if (emailUser.Text != String.Empty)
            {
                if (UserTypeChoice.registrationService.SetUserEmail(emailUser.Text) == true)
                {
                    this.NavigationService.Navigate(new Uri("pack://application:,,,/Registration/Password.xaml"));
                }
                else
                {
                    MessageBox.Show("Данный пользователь уже существует. Введите другой адрес.");
                }
            }
            else
            {
                MessageBox.Show("Ошибка! Повторите ещё раз.");
            }
        }
    }
}
