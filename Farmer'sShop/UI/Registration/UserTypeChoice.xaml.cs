﻿using FarmersShop.Services;
using System;
using System.Windows;

namespace UI.Registration
{
    public partial class UserTypeChoice : Window
    {
        public static RegistrationService registrationService;

        public UserTypeChoice()
        {
            InitializeComponent();


        }
        /*    Pages.Add();
            Pages.Add(new Uri("pack://application:,,,/Registration/Password.xaml"));
            Pages.Add(new Uri("pack://application:,,,/Registration/Nickname.xaml"));
            Pages.Add(new Uri("pack://application:,,,/Registration/Address.xaml"));*/
        private void NextClick(object sender, RoutedEventArgs e)
        {

            typeChoise.Visibility = Visibility.Hidden;
            typeTitle.Visibility = Visibility.Hidden;
            nextButton.Visibility=Visibility.Hidden;
            registrationService = new RegistrationService();
            registrationService.SetUserType(typeChoise.SelectedIndex + 1);
            frame.NavigationUIVisibility = System.Windows.Navigation.NavigationUIVisibility.Hidden;
            frame.NavigationService.Navigate(new Uri("pack://application:,,,/Registration/Email.xaml"));

        }
    }
}
