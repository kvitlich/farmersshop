﻿using FarmersShop.Services.GeolocationService;
using Newtonsoft.Json;
using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace UI.Registration
{
    public partial class Address : Page
    {
        private const string apiKey = "AIzaSyCJOLbOQPlRzuVe1B2IsjKC7OweXY4o2xs";
        private static string baseUrlGC = "https://maps.googleapis.com/maps/api/geocode/json?address=";
        private static string plusUrl = "&key=" + apiKey + "&sensor=false";
        private static bool geoStatus = false;
        private static string latitude;
        private static string longitude;

        public Address()
        {
            InitializeComponent();
        }

        private void AdressUserKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                if (adressUser.Text != String.Empty)
                {
                    GeoCoding(adressUser.Text);
                    if (geoStatus == true)
                    {
                        map.Navigate($"https://www.google.com/maps/place/{latitude}+{longitude}");
                        doneButton.Visibility = Visibility.Visible;
                    }
                    else
                    {
                        MessageBox.Show("Ошибка! Повторите ещё раз.");
                    }
                }

            }
        }

        private static void GeoCoding(string address)
        {
            geoStatus = false;

            var json = new WebClient().DownloadString(baseUrlGC + address.Replace(" ", "+") + plusUrl);
            GoogleGeoCodeResponse jsonResult = JsonConvert.DeserializeObject<GoogleGeoCodeResponse>(json);

            string status = jsonResult.status;

            if (status == "OK")
            {
                geoStatus = true;
                for (int i = 0; i < jsonResult.results.Length; i++)
                {
                    latitude = jsonResult.results[i].geometry.location.lat;
                    longitude = jsonResult.results[i].geometry.location.lng + Environment.NewLine;
                }
            }
        }

        private void EndButtonClick(object sender, RoutedEventArgs e)
        {
            if (geoStatus == true)
            {
                UserTypeChoice.registrationService.SetUserAddress(adressUser.Text);
                UserTypeChoice.registrationService.AddUser();
                StarterPage starterPage = new StarterPage();
                Application.Current.MainWindow = starterPage;
                Application.Current.Windows[0].Close();
                starterPage.Show();
            }
        }
    }
}
