﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI
{
    /// <summary>
    /// Логика взаимодействия для ProductsCategories.xaml
    /// </summary>
    public partial class ProductsCategories : Page
    {
        private ObservableCollection<ProductCategory> categories = new ObservableCollection<ProductCategory>();

        public ProductsCategories()
        {
            InitializeComponent();
            using (var context = new FarmersShopContext())
            {
                categories = new ObservableCollection<ProductCategory>(context.ProductCategories.ToList());
            }
            dataGrid.ItemsSource = categories;
        }
    }
}
