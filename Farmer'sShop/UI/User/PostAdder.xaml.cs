﻿using FarmersShop.Domain;
using FarmersShop.Services;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI.User
{
    public partial class PostAdder : Page
    {
        public PostAdder()
        {
            InitializeComponent();
        }

        private void AddPost(object sender, RoutedEventArgs e)
        {
            if (titleTB.Text != String.Empty && descriptionTB.Text != String.Empty)
            {
                using (var context = new FarmersShopContext())
                {
                    OtherInfo otherInfo = new OtherInfo
                    {
                        Title = titleTB.Text,
                        Description = descriptionTB.Text,
                        IsAcceptable = true
                    };

                    if (Account.currentUser.Type == 2)
                    {
                        var publisherSeller = context.Sellers.Where(s => s.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                        otherInfo.PublisherSeller = publisherSeller;
                        context.OtherInfos.Add(otherInfo);
                        context.SaveChanges();
                        this.NavigationService.Navigate(new Uri("pack://application:,,,/Seller/SellerPersonalAccount.xaml"));
                    }

                    else if (Account.currentUser.Type == 3)
                    {
                        var publisherBuyer = context.Buyers.Where(b => b.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                        otherInfo.PublisherBuyer = publisherBuyer;
                        context.OtherInfos.Add(otherInfo);
                        context.SaveChanges();
                        this.NavigationService.Navigate(new Uri("pack://application:,,,/Buyer/PersonalAccount.xaml"));
                    }                    
                }
            }
            else
            {
                errorMessage.Text = "Заполните все поля!";
            }
        }
    }
}
