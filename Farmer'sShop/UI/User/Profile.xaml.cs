﻿using FarmersShop.Services;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI.Seller
{
    public partial class Profile : Page
    {
        private string nickname;
        private string adress;
        private string email;
        private string password;
        private string passwordVisible;

        public Profile()
        {
            InitializeComponent();

            using (var context = new FarmersShopContext()) // 1 - Администратор, 2 - Продавец, 3 - Покупатель
            {
                    string passwordS = String.Empty;
                if (Account.currentUser.Type == 1)
                {
                    var currentAdministrator = context.Administrators.Where(s => s.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                    nickname = currentAdministrator.Nickname;
                    adress = currentAdministrator.Adress;
                    email = currentAdministrator.Email;
                    password = passwordS.PadLeft(currentAdministrator.Password.Length, '*');
                    passwordVisible = currentAdministrator.Password;
                }
                else if (Account.currentUser.Type == 2)
                {
                    var currentSeller = context.Sellers.Where(s => s.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                    nickname = currentSeller.Nickname;
                    adress = currentSeller.Adress;
                    email = currentSeller.Email;
                    password = passwordS.PadLeft(currentSeller.Password.Length, '*');
                    passwordVisible = currentSeller.Password;
                }
                else if (Account.currentUser.Type == 3)
                {
                    var currentBuyer = context.Buyers.Where(s => s.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                    nickname = currentBuyer.Nickname;
                    adress = currentBuyer.Adress;
                    email = currentBuyer.Email;
                    password = passwordS.PadLeft(currentBuyer.Password.Length, '*');
                    passwordVisible = currentBuyer.Password;
                }
            }
            Equate();
        }

        private void Equate()
        {
            nicknameTextBox.Text = nickname;
            addressTextBox.Text = adress;
            emailTextBox.Text = email;
            passwordTextBox.Text = password;
        }

        private void EditButtonClick(object sender, RoutedEventArgs e)
        {
            editButton.Visibility = Visibility.Hidden;
            readyButton.Visibility = Visibility.Visible;
            cancelButton.Visibility = Visibility.Visible;
            nicknameTextBox.IsReadOnly = false;
            addressTextBox.IsReadOnly = false;
            emailTextBox.IsReadOnly = false;
            passwordTextBox.IsReadOnly = false;
            passwordTextBox.Text = passwordVisible;
        }

        private void ReadyButtonClick(object sender, RoutedEventArgs e)
        {
            editButton.Visibility = Visibility.Visible;
            readyButton.Visibility = Visibility.Hidden;
            cancelButton.Visibility = Visibility.Hidden;
            nicknameTextBox.IsReadOnly = true;
            addressTextBox.IsReadOnly = true;
            emailTextBox.IsReadOnly = true;
            passwordTextBox.IsReadOnly = true;
            nickname = nicknameTextBox.Text;
            adress = addressTextBox.Text;
            email = emailTextBox.Text;
            password = passwordTextBox.Text;

            Equate();

            using (var context = new FarmersShopContext())
            {
                if (Account.currentUser.Type == 1)
                {
                    var currentAdmin = context.Administrators.Where(s => s.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                    currentAdmin.Nickname = nickname;
                    currentAdmin.Adress = adress;
                    currentAdmin.Email = email;
                    currentAdmin.Password = password;
                }
                else if (Account.currentUser.Type == 2)
                {
                    var currentSeller = context.Sellers.Where(s => s.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                    currentSeller.Nickname = nickname;
                    currentSeller.Adress = adress;
                    currentSeller.Email = email;
                    currentSeller.Password = password;
                }
                else if (Account.currentUser.Type == 3)
                {
                    var currentBuyer = context.Buyers.Where(s => s.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                    currentBuyer.Nickname = nickname;
                    currentBuyer.Adress = adress;
                    currentBuyer.Email = email;
                    currentBuyer.Password = password;
                }
                context.SaveChanges();
            }            
        }

        private void CancelButtonClick(object sender, RoutedEventArgs e)
        {
            editButton.Visibility = Visibility.Visible;
            readyButton.Visibility = Visibility.Hidden;
            cancelButton.Visibility = Visibility.Hidden;
            nicknameTextBox.IsReadOnly = true;
            addressTextBox.IsReadOnly = true;
            emailTextBox.IsReadOnly = true;
            passwordTextBox.IsReadOnly = true;
            Equate();
        }
    }
}
