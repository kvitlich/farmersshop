﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI.User
{
    public partial class Posts : Page
    {
        private ObservableCollection<OtherInfo> posts;
        public static int selectedIndex;

        public Posts()
        {
            InitializeComponent();

            using (var context = new FarmersShopContext())
            {
                posts = new ObservableCollection<OtherInfo>(context.OtherInfos.Where(i => i.DeletedDate == null).OrderByDescending(i=>i.CretionDate).ThenBy(i=>i.Title));
            }
            postsList.ItemsSource = posts;
        }

        private void WatchThePost(object sender, RoutedEventArgs e)
        {
            selectedIndex = postsList.SelectedIndex;
            this.NavigationService.Navigate(new Uri("pack://application:,,,/User/PostData.xaml"));
        }
    }
}
