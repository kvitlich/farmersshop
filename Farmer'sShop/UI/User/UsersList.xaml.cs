﻿using FarmersShop.Services;
using FarmersShop.ShopDbContext;
using System.Linq;
using System.Windows.Controls;
using FarmersShop.Domain;
using FarmersShop.Services.ChatService;
using Microsoft.Data.SqlClient;
using System;
using System.Windows.Threading;

namespace UI.User
{
    /// <summary>
    /// Interaction logic for UsersList.xaml
    /// </summary>
    public partial class UsersList : Page
    {
        private FarmersShop.Domain.User lastUser;
        public UsersList()
        {
            InitializeComponent();
            using (var context = new FarmersShopContext())
            {
                if (Account.currentUser.Type == 1)
                {
                    dataGrid.ItemsSource = context.Users.ToList();
                }
                else if (Account.currentUser.Type == 2) //2 - продавцы, им нужны покупатели
                {
                    dataGrid.ItemsSource = context.Buyers.ToList();
                }
                else if (Account.currentUser.Type == 3)//3 - покупатели, им нужны продавцы
                {
                    dataGrid.ItemsSource = context.Sellers.ToList();
                }
            }
        }

        private void findButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            using (var context = new FarmersShopContext())
            {
                if (Account.currentUser.Type == 1)
                {
                    if (findTextBox.Text == "" && findTextBox.Text == null)
                        dataGrid.ItemsSource = context.Users.ToList();
                    else
                        dataGrid.ItemsSource = context.Users.Where(x => x.Nickname.Contains(findTextBox.Text)).ToList();
                }
                else if (Account.currentUser.Type == 2) //2 - продавцы, им нужны покупатели
                {

                    if (findTextBox.Text == "" && findTextBox.Text == null)
                        dataGrid.ItemsSource = context.Buyers.ToList();
                    else
                        dataGrid.ItemsSource = context.Buyers.Where(x => x.Nickname.Contains(findTextBox.Text)).ToList();
                }
                else if (Account.currentUser.Type == 3)//3 - покупатели, им нужны продавцы
                {

                    if (findTextBox.Text == "" && findTextBox.Text == null)
                        dataGrid.ItemsSource = context.Sellers.ToList();
                    else
                        dataGrid.ItemsSource = context.Sellers.Where(x => x.Nickname.Contains(findTextBox.Text)).ToList();
                }
            }
        }

        private void messageButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            if (lastUser != null)
                {
                using (var context = new FarmersShopContext())
                {
                    FarmersShop.Domain.User sourseUser;
                    var currentUserType = Account.currentUser.Type;
                    MessageExplorer messageExplorer = new MessageExplorer();
                    if (currentUserType == 1)
                    {
                        sourseUser = (FarmersShop.Domain.User)dataGrid.SelectedItem;
                    }
                    else if (currentUserType == 2) //2 - продавцы, им нужны покупатели || 3 - покупатели, им нужны продавцы
                    {
                        var from = context.Sellers.Where(x => x.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                        var to = context.Buyers.Where(x => x.Id.Equals(lastUser.EntityId)).FirstOrDefault();
                        messageExplorer.SendMessage(from, to, messageTextBox.Text);
                        chatDataGrid.ItemsSource = context.ChatMessages.Where(x => x.FromUser.EntityId.Equals(to.Id) || x.Chat.Buyer.Id.Equals(Account.currentUser.EntityId) && x.Chat.Seller.Id.Equals(to.Id) && x.DeletedDate == null).ToList();
                    }
                    else if (currentUserType == 3) //3 - покупатели, им нужны продавцы
                    {
                        var from = context.Buyers.Where(x => x.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                        var to = context.Sellers.Where(x => x.Id.Equals(lastUser.EntityId)).FirstOrDefault();
                        messageExplorer.SendMessage(from, to, messageTextBox.Text);
                        chatDataGrid.ItemsSource = context.ChatMessages.Where(x => x.FromUser.EntityId.Equals(to.Id) || x.Chat.Buyer.Id.Equals(to.Id) && x.Chat.Seller.Id.Equals(Account.currentUser.EntityId) && x.DeletedDate == null).ToList();
                    }
                    messageTextBox.Text = "";
                }
            }
        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            using (var context = new FarmersShopContext())
            {
                lastUser = context.Users.Where(x => x.EntityId.Equals(dataGrid.SelectedItem)).FirstOrDefault();
                MessageExplorer messageExplorer = new MessageExplorer();
                var currentUserType = Account.currentUser.Type;
                if (currentUserType == 1)
                {
                    //  chatDataGrid.ItemsSource = context.ChatMessages.Where(x => x.FromUser.EntityId.Equals(fromMessagging.EntityId) && x.Chat.Buyer.Equals());

                }
                else if (currentUserType == 2) //2 - продавцы, им нужны покупатели || 3 - покупатели, им нужны продавцы
                {
                    var destMessage = (FarmersShop.Domain.Buyer)dataGrid.SelectedItem;
                    var currentSeller = context.Sellers.Where(x => x.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                    messageExplorer.CreateCheckChat(destMessage, currentSeller);
                    chatDataGrid.ItemsSource = context.ChatMessages.Where(x => x.FromUser.EntityId.Equals(destMessage.Id) && x.Chat.Buyer.Id.Equals(Account.currentUser.EntityId) && x.Chat.Seller.Id.Equals(destMessage.Id) && x.DeletedDate==null).ToList();
                }
                else if (currentUserType == 3) //3 - покупатели, им нужны продавцы
                {
                    var destMessage = (FarmersShop.Domain.Seller)dataGrid.SelectedItem;
                    var currentBuyer = context.Buyers.Where(x => x.Id.Equals(Account.currentUser.EntityId)).FirstOrDefault();
                    messageExplorer.CreateCheckChat(currentBuyer, destMessage);
                    chatDataGrid.ItemsSource = context.ChatMessages.Where(x => x.FromUser.EntityId.Equals(destMessage.Id) && x.Chat.Buyer.Id.Equals(destMessage.Id) && x.Chat.Seller.Id.Equals(Account.currentUser.EntityId) && x.DeletedDate == null).ToList(); ;
                }
            }
        }
    }
}
