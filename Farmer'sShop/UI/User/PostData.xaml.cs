﻿using FarmersShop.Services;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI.User
{
    public partial class PostData : Page
    {
        public PostData()
        {
            InitializeComponent();

            using (var context = new FarmersShopContext())
            {
                var post = context.OtherInfos.Where(i => i.DeletedDate == null).OrderBy(i => i.Title).Skip(Posts.selectedIndex).Take(1).FirstOrDefault();
                var publisherBuyerNickname = context.OtherInfos.OrderBy(i => i.Title).Select(i => i.PublisherBuyer.Nickname).ToList();
                var publisherSellerNickname = context.OtherInfos.OrderBy(i => i.Title).Select(i => i.PublisherSeller.Nickname).ToList();

                titleTB.Text = post.Title;
                publisherTB.Text = (publisherBuyerNickname[Posts.selectedIndex] != null) ?
                    publisherBuyerNickname[Posts.selectedIndex] : publisherSellerNickname[Posts.selectedIndex];
                dateTB.Text = post.CretionDate.ToString();
                descriptionTB.Text = post.Description;
            }
        }

        private void SubscribeToThePost(object sender, RoutedEventArgs e)
        {
            if (Account.currentUser.Type == 2)
            {

            }
            else if (Account.currentUser.Type == 3)
            {

            }
        }
    }
}
