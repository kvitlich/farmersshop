﻿using FarmersShop.Domain;
using FarmersShop.Services;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace UI.Seller
{
    public partial class ProductDelete : Page
    {
        private ObservableCollection<Product> products = new ObservableCollection<Product>();

        public ProductDelete()
        {
            InitializeComponent();
            using (var context = new FarmersShopContext())
            {
                products = new ObservableCollection<Product>(context.Products
                    .Where(p => p.Seller.Id.Equals(Account.currentUser.EntityId) && p.DeletedDate == null)
                    .OrderBy(p => p.ProductName).ToList());
            }
            comboBox.ItemsSource = products;
        }

        private void DeleteProduct(object sender, RoutedEventArgs e)
        {
            using (var context = new FarmersShopContext())
            {
                var productToDelete = context.Products
                    .Where(p => p.Seller.Id.Equals(Account.currentUser.EntityId) && p.DeletedDate == null)
                    .OrderBy(p => p.ProductName).ToList();
                productToDelete.FirstOrDefault().DeletedDate = DateTime.Now;
                context.SaveChanges();
            }
            this.NavigationService.Navigate(new Uri("pack://application:,,,/Seller/SellerPersonalAccount.xaml"));
        }
    }
}
