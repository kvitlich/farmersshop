﻿using FarmersShop.Services;
using FarmersShop.ShopDbContext;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace UI.Seller
{
    public partial class OrdersCompleted : Page
    {
        private void RefreshData()
        {
            using (var context = new FarmersShopContext())
            {
                var temp = from buyer in context.Buyers
                           join order in context.Orders on buyer.Id equals order.Buyer.Id
                           join product in context.Products on order.Product.Id equals product.Id
                           where (product.Seller.Id.Equals(Account.currentUser.EntityId) && order.CheckedDate != null && order.DeletedDate == null)
                           select new
                           {
                               buyer = buyer.Nickname,
                               kgs = order.Kgs,
                               productName = product.ProductName,
                               orderId = order.Id,
                               sentDate = order.CheckedDate
                           };

                var orders = temp.OrderByDescending(t=>t.sentDate).ToList();
                ordersList.ItemsSource = orders;
            }
        }

        public OrdersCompleted()
        {
            InitializeComponent();
            RefreshData();
        }
    }
}
