﻿using FarmersShop.ShopDbContext;
using System;
using System.Windows;

namespace UI.Seller
{
    public partial class SellerWindow : Window
    {
        public SellerWindow()
        {
            InitializeComponent();
        }
        private void OpenMenuButtonClick(object sender, RoutedEventArgs e)
        {
            openMenuButton.Visibility = Visibility.Collapsed;
            closeMenuButton.Visibility = Visibility.Visible;
        }

        private void CloseMenuButtonClick(object sender, RoutedEventArgs e)
        {
            closeMenuButton.Visibility = Visibility.Collapsed;
            openMenuButton.Visibility = Visibility.Visible;
        }

        private void DatebaseButtonClick(object sender, RoutedEventArgs e)
        {
            TablesCreator.Fill();
        }

        private void CategoryMenuItemClick(object sender, RoutedEventArgs e)
        {

        }

        private void SubscribeMenuItemClick(object sender, RoutedEventArgs e)
        {

        }

        private void ChatMenuItemClick(object sender, RoutedEventArgs e)
        {

        }

        private void SearchMenuItemClick(object sender, RoutedEventArgs e)
        {
            frame.Navigate(new Uri("pack://application:,,,/SearchPage.xaml"));
        }

        private void CartMenuItemClick(object sender, RoutedEventArgs e)
        {
            frame.Navigate(new Uri("pack://application:,,,/Seller/OrdersChoise.xaml"));
        }

        private void UserMenuItemClick(object sender, RoutedEventArgs e)
        {
            frame.Navigate(new Uri("pack://application:,,,/Seller/SellerPersonalAccount.xaml"));
        }

        private void ExitButtonClick(object sender, RoutedEventArgs e)
        {
            StarterPage starterPage = new StarterPage();
            Application.Current.MainWindow = starterPage;
            Application.Current.Windows[0].Close();
            starterPage.Show();
        }

        private void PostsButtonClick(object sender, RoutedEventArgs e)
        {
            frame.Navigate(new Uri("pack://application:,,,/User/Posts.xaml"));
        }
    }
}
