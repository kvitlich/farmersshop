﻿using FarmersShop.Services;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace UI.Seller
{
    public partial class Orders : Page
    {
        private void RefreshData()
        {
            using (var context = new FarmersShopContext())
            {
                var temp = from buyer in context.Buyers
                           join order in context.Orders on buyer.Id equals order.Buyer.Id
                           join product in context.Products on order.Product.Id equals product.Id
                           where (product.Seller.Id.Equals(Account.currentUser.EntityId) && order.CheckedDate == null && order.DeletedDate == null)
                           select new
                           {
                               buyer = buyer.Nickname,
                               kgs = order.Kgs,
                               productName = product.ProductName,
                               orderId = order.Id,
                               orderDate = order.CretionDate
                           };

                var orders = temp.OrderBy(t=>t.orderDate).ToList();
                ordersList.ItemsSource = orders;
            }
        }

        public Orders()
        {
            InitializeComponent();
            RefreshData();
        }

        private void SentButtonClick(object sender, RoutedEventArgs e)
        {
            using (var context = new FarmersShopContext())
            {
                int selectedIndex = ordersList.SelectedIndex;

                var temp = from buyer in context.Buyers
                           join order in context.Orders on buyer.Id equals order.Buyer.Id
                           join product in context.Products on order.Product.Id equals product.Id
                           where (product.Seller.Id.Equals(Account.currentUser.EntityId) && order.CheckedDate == null && order.DeletedDate == null)
                           select new
                           {
                               kgs = order.Kgs,
                               productName = product.ProductName,
                               orderId = order.Id
                           };
                var orders = temp.ToList();

                var selectedOrder = orders.Skip(selectedIndex).Take(1);

                var orderDB = context.Orders.Where(o => o.Id.Equals(selectedOrder.FirstOrDefault().orderId));
                orderDB.FirstOrDefault().CheckedDate = DateTime.Now;

                var productDB = context.Products.Where(p => p.ProductName.Equals(selectedOrder.FirstOrDefault().productName));
                productDB.FirstOrDefault().Kg -= selectedOrder.FirstOrDefault().kgs;

                context.SaveChanges();
            }
            RefreshData();
        }
    }
}
