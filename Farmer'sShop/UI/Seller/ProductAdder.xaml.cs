﻿using FarmersShop.Domain;
using FarmersShop.Services;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace UI.Seller
{
    public partial class ProductAdder : Page
    {
        private ObservableCollection<ProductCategory> productCategories = new ObservableCollection<ProductCategory>();

        public ProductAdder()
        {
            InitializeComponent();
            using (var context = new FarmersShopContext())
            {
                productCategories = new ObservableCollection<ProductCategory>(context.ProductCategories.OrderBy(p => p.ProductCategoryName).ToList());
            }
            comboBox.ItemsSource = productCategories;
        }

        private void AddProductButtonClick(object sender, RoutedEventArgs e)
        {
            if (nameTB.Text != String.Empty && descriptionTB.Text != String.Empty && imageTB.Text != String.Empty &&
                priceTB.Text != String.Empty && kgTB.Text != String.Empty && discountTB.Text != String.Empty)
            {
                int price, kg, discount;
                bool parsedPrice = Int32.TryParse(priceTB.Text, out price);
                bool parsedKg = Int32.TryParse(kgTB.Text, out kg);
                bool parsedDiscount = Int32.TryParse(discountTB.Text, out discount);

                if (parsedPrice == true && parsedKg == true && parsedDiscount == true)
                {
                    using (var context = new FarmersShopContext())
                    {
                        var allProductCategories = context.ProductCategories.OrderBy(p => p.ProductCategoryName).ToList();
                        var productCategory = allProductCategories.Skip(comboBox.SelectedIndex).Take(1);
                        var seller = context.Sellers.Where(s => s.Id.Equals(Account.currentUser.EntityId));

                        Product product = new Product
                        {
                            ProductName = nameTB.Text,
                            Description = descriptionTB.Text,
                            Seller = seller.FirstOrDefault(),
                            ProductCategory = productCategory.FirstOrDefault(),
                            PricePerKg = Convert.ToInt32(priceTB.Text),
                            Kg = Convert.ToInt32(kgTB.Text),
                            Discount = Convert.ToInt32(discountTB.Text),
                            Image = imageTB.Text
                        };

                        context.Products.Add(product);
                        context.SaveChanges();
                    }
                    this.NavigationService.Navigate(new Uri("pack://application:,,,/Seller/SellerPersonalAccount.xaml"));
                }
                else
                {
                    if (parsedPrice == false) errorMessage.Text = "Ошибка в заполнении поля — Цена за кг";
                    else if (parsedKg == false) errorMessage.Text = "Ошибка в заполнении поля — Кг";
                    else if (parsedDiscount == false) errorMessage.Text = "Ошибка в заполнении поля — Скидка (%)";
                }                
            }
            else
            {
                errorMessage.Text = "Заполните все поля!";
            }            
        }
    }
}
