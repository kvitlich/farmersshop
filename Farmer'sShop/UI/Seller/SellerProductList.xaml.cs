﻿using FarmersShop.Domain;
using FarmersShop.Services;
using FarmersShop.ShopDbContext;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;

namespace UI.Seller
{
    public partial class SellerProductList : Page
    {
        private ObservableCollection<Product> products = new ObservableCollection<Product>();

        public SellerProductList()
        {
            InitializeComponent();

            using (var context = new FarmersShopContext())
            {
                products = new ObservableCollection<Product>(context.Products
                    .Where(p => p.Seller.Id.Equals(Account.currentUser.EntityId) && p.DeletedDate==null).ToList());
            }
            dataGrid.ItemsSource = products;
        }
    }
}
