﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace UI.Seller
{
    public partial class SellerPersonalAccount : Page
    {
        public SellerPersonalAccount()
        {
            InitializeComponent();
        }

        private void ProductsList(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/Seller/SellerProductList.xaml"));
        }

        private void AddProduct(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/Seller/ProductAdder.xaml"));
        }
        private void DeleteProduct(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/Seller/ProductDelete.xaml"));
        }

        private void Profile(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/User/Profile.xaml"));
        }

        private void AddPost(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/User/PostAdder.xaml"));
        }
    }
}
