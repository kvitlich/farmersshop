﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI.Seller
{
    public partial class OrdersChoise : Page
    {
        public OrdersChoise()
        {
            InitializeComponent();
        }

        private void CurrentOrders(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/Seller/Orders.xaml"));
        }

        private void CompletedOrders(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/Seller/OrdersCompleted.xaml"));
        }
    }
}
