﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI.Buyer
{
    public partial class SubscribedCategoriesProducts : Page
    {
        private ObservableCollection<Product> productsBySubscription;

        public SubscribedCategoriesProducts()
        {
            InitializeComponent();
            using (var context = new FarmersShopContext())
            {
                var currentBuyer = context.Buyers.Where(b => b.Id.Equals(FarmersShop.Services.Account.currentUser.EntityId)).FirstOrDefault();
                var result = context.Products.Where(x => x.ProductCategory.BuyersSubscriptions.Any(y => y.Buyer.Equals(currentBuyer) && y.DeletedDate == null));

                productsBySubscription = new ObservableCollection<Product>(result);
            }
            subscribedProductsByList.ItemsSource = productsBySubscription;
        }
    }
}
