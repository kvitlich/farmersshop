﻿using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace UI.Buyer
{
    public partial class BuyerWindow : Window
    {
        public BuyerWindow()
        {
            InitializeComponent();
        }

        private void OpenMenuButtonClick(object sender, RoutedEventArgs e)
        {
            OpenMenu();
        }

        private void OpenMenu()
        {
            openMenuButton.Visibility = Visibility.Collapsed;
            closeMenuButton.Visibility = Visibility.Visible;
        }

        private void CloseMenuButtonClick(object sender, RoutedEventArgs e)
        {
            CloseMenu();
        }

        private void CloseMenu()
        {
            closeMenuButton.Visibility = Visibility.Collapsed;
            openMenuButton.Visibility = Visibility.Visible;
        }

        private void DatebaseButtonClick(object sender, RoutedEventArgs e)
        {
            TablesCreator.Fill();
        }

        private void CategoryMenuItemClick(object sender, RoutedEventArgs e)
        {
            CloseMenu();
            frame.Navigate(new Uri("pack://application:,,,/Buyer/Categories.xaml"));
        }

        private void SubscribeMenuItemClick(object sender, RoutedEventArgs e)
        {
            CloseMenu();
        }

        private void ChatMenuItemClick(object sender, RoutedEventArgs e)
        {
            CloseMenu();
            frame.Navigate(new Uri("pack://application:,,,/User/UsersList.xaml"));
        }

        private void SearchMenuItemClick(object sender, RoutedEventArgs e)
        {
            CloseMenu(); 
            frame.Navigate(new Uri("pack://application:,,,/Buyer/BuyerProductList.xaml"));
        }

        private void CartMenuItemClick(object sender, RoutedEventArgs e)
        {
            CloseMenu();
            frame.Navigate(new Uri("pack://application:,,,/Buyer/BuyersBasket.xaml"));

        }

        private void UserMenuItemClick(object sender, RoutedEventArgs e)
        {
            CloseMenu();
            frame.Navigate(new Uri("pack://application:,,,/Buyer/PersonalAccount.xaml"));
        }

        private void ExitButtonClick(object sender, RoutedEventArgs e)
        {
            StarterPage starterPage = new StarterPage();
            Application.Current.MainWindow = starterPage;
            Application.Current.Windows[0].Close();
            starterPage.Show();
        }

        private void PostsButtonClick(object sender, RoutedEventArgs e)
        {
            frame.Navigate(new Uri("pack://application:,,,/User/Posts.xaml"));
        }
    }
}
