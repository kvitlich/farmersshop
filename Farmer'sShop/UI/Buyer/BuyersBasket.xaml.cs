﻿using FarmersShop.Domain;
using FarmersShop.Services.OrderService;
using FarmersShop.ShopDbContext;
using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace UI.Buyer.Account
{
    public partial class BuyersBasket : Page
    {
        public BasketRefresher basketRefresher;

        public void Refresh()
        {
            using (var context = new FarmersShopContext())
            {
                basketRefresher = new BasketRefresher(context.Buyers.Where(x => x.Id.Equals(FarmersShop.Services.Account.currentUser.EntityId)).FirstOrDefault());
                dataGrid.ItemsSource = basketRefresher.GetProducts();
            }
        }

        public BuyersBasket()
        {
            InitializeComponent();
            Refresh();
        }

        private void BuyProduct(object sender, RoutedEventArgs e)
        {
            int parsingResult;

            if (dataGrid.SelectedItem != null && Int32.TryParse(Kgs_TextBox.Text, out parsingResult))
            {
                using (var context = new FarmersShopContext())
                {
                    int selectedIndex = dataGrid.SelectedIndex;
                    basketRefresher = new BasketRefresher(context.Buyers.Where(x => x.Email.Equals(FarmersShop.Services.Account.currentUser.Email)).FirstOrDefault());
                    var allProducts = basketRefresher.GetProducts();
                    if (parsingResult <= allProducts.Skip(selectedIndex).Take(1).FirstOrDefault().Kg)
                    {
                        Order order = new Order
                        {
                            Buyer = context.Buyers.Where(b => b.Id.Equals(FarmersShop.Services.Account.currentUser.EntityId)).FirstOrDefault(),
                            Product = context.Products.Where(p=>p.Id.Equals(allProducts.Skip(selectedIndex).Take(1).FirstOrDefault().Id)).FirstOrDefault(),
                            Kgs = parsingResult,
                            DeletedDate = DateTime.Now
                        };
                        var product = context.Products.Where(p => p.Id.Equals(allProducts.Skip(selectedIndex).Take(1).FirstOrDefault().Id)).FirstOrDefault();
                        product.Kg -= parsingResult;

                        context.Orders.Add(order);
                        context.SaveChanges();
                        Refresh();
                    }
                    else
                    {
                        errorMessage.Text = "Вы ввели больше кг, чем доступно у этого продукта.";
                    }
                };
            }
            else
            {
                errorMessage.Text = "Выберите продукт и введите количесво!";
            }
        }

        private void Find_Button_Click(object sender, RoutedEventArgs e)
        {

            using (var context = new FarmersShopContext())
            {

            }

        }

        private void dataGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Delete_Button_Click(object sender, RoutedEventArgs e)
        {
            var prodTodelete = (Product)dataGrid.SelectedItem;
            basketRefresher.DeleteFromBasket(prodTodelete);
            dataGrid.ItemsSource = basketRefresher.GetProducts();
        }
    }
}
