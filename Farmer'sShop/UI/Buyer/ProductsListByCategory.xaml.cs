﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI.Buyer
{
    public partial class ProductsListByCategory : Page
    {
        private ObservableCollection<Product> productsByCategory = new ObservableCollection<Product>();
        public ProductsListByCategory()
        {
            InitializeComponent();

            using (var context = new FarmersShopContext())
            {
                var selectedCategory = context.ProductCategories.Where(c => c.DeletedDate == null)
                    .OrderBy(c => c.ProductCategoryName).Skip(Categories.selectedIndex).Take(1);
                productsByCategory = new ObservableCollection<Product>(context.Products
                    .Where(p => p.ProductCategory.Equals(selectedCategory.FirstOrDefault())).OrderBy(p => p.ProductName));
            }
            productsByCategoryList.ItemsSource = productsByCategory;
        }
    }
}
