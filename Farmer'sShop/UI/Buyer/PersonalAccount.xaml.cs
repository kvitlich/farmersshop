﻿using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI.Buyer
{
    public partial class PersonalAccount : Page
    {
        public PersonalAccount()
        {
            InitializeComponent();
        }

        private void OrderButtonClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/Buyer/Account/Orders.xaml"));
        }

        private void SubscriptionsButtonClick(object sender, RoutedEventArgs e)
        {
            //this.NavigationService.Navigate(new Uri("pack://application:,,,/Buyer/Subscriptions.xaml"));
            this.NavigationService.Navigate(new Uri("pack://application:,,,/Buyer/SubscribedCategoriesProducts.xaml"));
        }

        private void UserAccountClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/User/Profile.xaml"));
        }

        private void AddPost(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/User/PostAdder.xaml"));
        }
    }
}
