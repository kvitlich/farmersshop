﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI.Buyer
{
    public partial class Subscriptions : Page
    {
        public Subscriptions()
        {
            InitializeComponent();
        }

        private void CategoriesButtonClick(object sender, RoutedEventArgs e)
        {
            this.NavigationService.Navigate(new Uri("pack://application:,,,/Buyer/SubscribedCategoriesProducts.xaml"));
        }
    }
}
