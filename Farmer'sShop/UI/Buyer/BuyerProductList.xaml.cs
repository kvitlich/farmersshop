﻿using FarmersShop.Domain;
using FarmersShop.Services.OrderService;
using FarmersShop.ShopDbContext;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace UI.Buyer
{

    public partial class BuyerProductList : Page
    {
        private BasketRefresher basketRefresher;
        public BuyerProductList()
        {
            InitializeComponent();
            using (var context = new FarmersShopContext())
            {
                basketRefresher = new BasketRefresher(context.Buyers.Where(x=>x.Id.Equals(FarmersShop.Services.Account.currentUser.EntityId)).FirstOrDefault());
                dataGrid.ItemsSource = context.Products.Where(x=>x.DeletedDate == null).ToList<Product>();
            }
        }

        private void ToBasketClick(object sender, RoutedEventArgs e)
        {
            var product = (Product)dataGrid.SelectedItem;
            if (basketRefresher.Add(product))
                MessageBox.Show($"{product.ProductName} добавлен");

        }

        private void Find_Click(object sender, RoutedEventArgs e)
        {
            using (var context = new FarmersShopContext())
            {
                dataGrid.ItemsSource = context.Products.Where(x => x.ProductName.Contains(Find_TextBlock.Text)).ToList<Product>();
            }
        }
    }
}

//using FarmersShop.Domain;
//using FarmersShop.ShopDbContext;
//using System;
//using System.Collections.Generic;
//using System.Collections.ObjectModel;
//using System.Data;
//using System.Linq;
//using System.Text;
//using System.Windows;
//using System.Windows.Controls;
//using System.Windows.Data;
//using System.Windows.Documents;
//using System.Windows.Input;
//using System.Windows.Media;
//using System.Windows.Media.Imaging;
//using System.Windows.Navigation;
//using System.Windows.Shapes;

//namespace UI.Buyer
//{
//  /// <summary>
//  /// Interaction logic for BuyerProductList.xaml
//  /// </summary>
//  public partial class BuyerProductList : Page
//  {
//    private ObservableCollection<Product> products = new ObservableCollection<Product>();
//    public BuyerProductList()
//    {
//      InitializeComponent();
//      using (FarmersShopContext context = new FarmersShopContext()) 
//      {
//        products = new ObservableCollection<Product>(context.Products.ToList());
//      }     
//      dataGrid.ItemsSource = products;
//    }

//    private void ToBasket_CLick(object sender, RoutedEventArgs e)
//    {
//      var product = (Product)dataGrid.SelectedItem;
//      MessageBox.Show(product.ProductName);
//    }

//    private void Find_Click(object sender, RoutedEventArgs e)
//    {
//      var products = dataGrid.ItemsSource.Cast<Product>().ToList();
//      dataGrid.ItemsSource = products.Where(x => x.ProductName.Contains(Find_TextBlock.Text)).ToList();
//    }

//    private void Find_TextBlock_TextChanged(object sender, TextChangedEventArgs e)
//    {
//      Find_TextBlock.Text = "";
//    }
//  }
//}
