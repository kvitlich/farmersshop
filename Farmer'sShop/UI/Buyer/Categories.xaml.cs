﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Controls;

namespace UI.Buyer
{
    public partial class Categories : Page
    {
        private ObservableCollection<ProductCategory> categories = new ObservableCollection<ProductCategory>();
        public static int selectedIndex;

        public Categories()
        {
            InitializeComponent();

            using (var context = new FarmersShopContext())
            {
                categories = new ObservableCollection<ProductCategory>(context.ProductCategories.Where(c => c.DeletedDate == null)
                    .OrderBy(c => c.ProductCategoryName).ToList());
            }
            productCategoriesList.ItemsSource = categories;
        }

        private void SubscribeToCategory(object sender, RoutedEventArgs e)
        {
            using (var context = new FarmersShopContext())
            {
                selectedIndex = productCategoriesList.SelectedIndex;
                var selectedCategory = categories.Skip(selectedIndex).Take(1);

                var sameSubscription = context.BuyersSubscriptions.Where(s => s.Buyer.Id
                .Equals(FarmersShop.Services.Account.currentUser.EntityId) && s.ProductCategory
                .Equals(selectedCategory.FirstOrDefault()) && s.DeletedDate == null).ToList();

                if (sameSubscription.Count == 0)
                {
                    var currentBuyer = context.Buyers.Where(b => b.Id.Equals(FarmersShop.Services.Account.currentUser.EntityId));
                    var currentCategory = context.ProductCategories.Where(c => c.Id.Equals(selectedCategory.FirstOrDefault().Id));
                    BuyersSubscriptions buyerSubscription = new BuyersSubscriptions
                    {
                        Buyer = currentBuyer.FirstOrDefault(),
                        ProductCategory = currentCategory.FirstOrDefault()
                    };
                    context.BuyersSubscriptions.Add(buyerSubscription);
                    context.SaveChanges();
                }
            }
        }

        private void WatchProductsByCategory(object sender, RoutedEventArgs e)
        {
            selectedIndex = productCategoriesList.SelectedIndex;
            this.NavigationService.Navigate(new Uri("pack://application:,,,/Buyer/ProductsListByCategory.xaml"));
        }

        private void UnsubscribeFromCategory(object sender, RoutedEventArgs e)
        {
            using (var context = new FarmersShopContext())
            {
                selectedIndex = productCategoriesList.SelectedIndex;
                var selectedCategory = categories.Skip(selectedIndex).Take(1);

                var sameSubscription = context.BuyersSubscriptions.Where(s => s.Buyer.Id
                .Equals(FarmersShop.Services.Account.currentUser.EntityId) && s.ProductCategory
                .Equals(selectedCategory.FirstOrDefault()) && s.DeletedDate == null).ToList();

                if (sameSubscription.Count > 0)
                {
                    sameSubscription.FirstOrDefault().DeletedDate = DateTime.Now;

                    context.SaveChanges();
                }
            }
        }
    }
}
