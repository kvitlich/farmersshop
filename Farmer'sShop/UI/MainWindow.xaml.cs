﻿using FarmersShop.Domain;
using FarmersShop.Services;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            
        }

        private void DATABASE_Click(object sender, RoutedEventArgs e)
        {
            TablesCreator.Fill();
        }

        private void UserCabButtonClick(object sender, RoutedEventArgs e)
        {
          
        }

        private void CartButtonClick(object sender, RoutedEventArgs e)
        {

  
        }

        private void SearchButtonClick(object sender, RoutedEventArgs e)
        {
        }

        private void chatMenuItem_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
