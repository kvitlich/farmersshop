﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System;
using System.Linq;

namespace FarmersShop.Services.ChatService
{
    public class MessageExplorer
    {
        public Chat CreateCheckChat(Buyer buyer, Seller seller)
        {
            Chat activatedChat;
            using (var context = new FarmersShopContext())
            {
                activatedChat = new Chat
                {
                    Buyer = buyer,
                    Seller = seller,
                };
                var chatConains = context.Chats.Where(x => x.Buyer.Id.Equals(buyer.Id) && x.Seller.Id.Equals(seller.Id));
                if (chatConains == null)
                { 
                    context.Chats.Add(activatedChat);
                    context.SaveChanges();
                }
            }
            return activatedChat;
        }

        public bool SendMessage(Buyer fromBuyer, Seller toSeller, string textMessage)// 1 - Администратор, 2 - Продавец, 3 - Покупатель
        {
            using (var context = new FarmersShopContext())
            {
                var fromUser = context.Users.Where(x => x.Type == 2 && x.EntityId.Equals(fromBuyer.Id)).SingleOrDefault();
                ChatMessages newChatMessage = new ChatMessages
                {
                    FromUser = fromUser,
                    Text = textMessage,
                    Chat = this.CreateCheckChat(fromBuyer, toSeller)
                };
                context.ChatMessages.Add(newChatMessage);
                context.SaveChanges();
            }
            return true;
        }

        public bool SendMessage(Seller fromSeller, Buyer toBuyer, string textMessage)// 1 - Администратор, 2 - Продавец, 3 - Покупатель
        {
            using (var context = new FarmersShopContext())
            {
                var fromUser = context.Users.Where(x => x.Type == 2 && x.EntityId.Equals(fromSeller.Id)).SingleOrDefault();
                ChatMessages newChatMessage = new ChatMessages
                {
                    FromUser = fromUser,
                    Text = textMessage,
                    Chat = this.CreateCheckChat(toBuyer,fromSeller)
                };
                context.ChatMessages.Add(newChatMessage);
                context.SaveChanges();
            }
            return true;
        }
    }
}
