﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System.Collections.Generic;
using System.Linq;

namespace FarmersShop.Services.ChatService
{
    public class UsersContext
    {

        public bool GetEntityByUser(User user, out List<Administrator> list)// 1 - Администратор, 2 - Продавец, 3 - Покупатель
        {
            list = null;
            if (user.Type != 1)
                return false;
            using (var context = new FarmersShopContext())
            {
                list = context.Administrators.Where(x => x.Id.Equals(user.EntityId)).ToList();
            }
            return true;
        }

        public bool GetEntityByUser(User user, out List<Seller> list)
        {
            list = null;
            if (user.Type != 2)
                return false;
            using (var context = new FarmersShopContext())
            {
                list = context.Sellers.Where(x => x.Id.Equals(user.EntityId)).ToList();
            }
            return true;
        }

        public bool GetEntityByUser(User user, out List<Buyer> list)
        {
            list = null;
            if (user.Type != 3)
                return false;
            using (var context = new FarmersShopContext())
            {
                list = context.Buyers.Where(x => x.Id.Equals(user.EntityId)).ToList();

            }
            return true;
        }
    }
}
