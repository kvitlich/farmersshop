﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System;
using System.Linq;

namespace FarmersShop.Services
{
    public class Account
    {
        public static User currentUser { get; set; }

        //public static User currentUser = new User
        //{
        //    EntityId = Guid.Parse("721CFD49-4362-4A39-B733-EC01679B07AC"),
        //    Email = "firstseller@gmail.com",
        //    Nickname = "First Seller",
        //    Type = 2
        //};

        //public static User currentUser = new User
        //{
        //    EntityId = Guid.Parse("093AE0C4-3D92-4988-9FE5-D1A7B42F6C0D"),
        //    Email = "secondbuyer@gmail.com",
        //    Nickname = "Second Buyer's Adress",
        //    Type = 3
        //};

        public int GetUserType()
        {
            return currentUser.Type;
        }

        public string GetUserEmail()
        {
            return currentUser.Email;
        }

        public void SetUserType(int type)
        {
            currentUser = new User();
            currentUser.Type = type;
        }

        public void SetUserEmail(string email)
        {
            currentUser.Email = email;
        }

        public void SetUserNicknameAndEntityId(string nickname)
        {
            currentUser.Nickname = nickname;
            if (currentUser.Type == 1)
            {
                using (var context = new FarmersShopContext())
                {
                    currentUser.EntityId = context.Administrators.Where(a => a.Email.Equals(currentUser.Email)).FirstOrDefault().Id;
                }
            }
            else if (currentUser.Type == 2)
            {
                using (var context = new FarmersShopContext())
                {
                    currentUser.EntityId = context.Sellers.Where(a => a.Email.Equals(currentUser.Email)).FirstOrDefault().Id;
                }
            }
            else if (currentUser.Type == 3)
            {
                using (var context = new FarmersShopContext())
                {
                    currentUser.EntityId = context.Buyers.Where(a => a.Email.Equals(currentUser.Email)).FirstOrDefault().Id;
                }
            }
        }
    }
}