﻿namespace FarmersShop.Services.GeolocationService
{
    public class AddressComponent
    {
        public string long_name { get; set; }
        public string short_name { get; set; }
        public string[] types { get; set; }
    }
}
