﻿namespace FarmersShop.Services.GeolocationService
{
    public class GoogleGeoCodeResponse
    {
        public string status { get; set; }
        public Results[] results { get; set; }
    }
}
