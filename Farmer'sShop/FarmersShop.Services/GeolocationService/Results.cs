﻿namespace FarmersShop.Services.GeolocationService
{
    public class Results
    {
        public string formatted_address { get; set; }
        public Geometry geometry { get; set; }
        public string[] types { get; set; }
        public AddressComponent[] address_components { get; set; }
    }
}
