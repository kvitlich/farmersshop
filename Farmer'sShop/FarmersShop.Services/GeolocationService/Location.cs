﻿namespace FarmersShop.Services.GeolocationService
{
    public class Location
    {
        public string lat { get; set; }
        public string lng { get; set; }
    }
}
