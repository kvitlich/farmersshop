﻿namespace FarmersShop.Services.GeolocationService
{
    public class Geometry
    {
        public string location_type { get; set; }
        public Location location { get; set; }
    }
}
