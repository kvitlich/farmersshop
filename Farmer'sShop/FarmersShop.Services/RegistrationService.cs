﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System.Linq;

namespace FarmersShop.Services
{
    public class RegistrationService
    {
        private User user;
        private Administrator administrator;
        private Seller seller;
        private Buyer buyer;
        private Account account;

        public void SetUserType(int type)
        {
            user = new User();
            user.Type = type;
        }

        public bool SetUserEmail(string email)
        {
            if (user.Type == 1)
            {
                using (var context = new FarmersShopContext())
                {
                    var currentEmail = context.Administrators.Where(a => a.Email.Equals(email)).ToList();
                    if (currentEmail.Count > 0)
                    {
                        return false;
                    }
                    else
                    {
                        administrator = new Administrator();
                        administrator.Email = email;
                        return true;
                    }
                }
            }
            else if (user.Type == 2)
            {
                using (var context = new FarmersShopContext())
                {
                    var currentEmail = context.Sellers.Where(a => a.Email.Equals(email)).ToList();
                    if (currentEmail.Count > 0)
                    {
                        return false;
                    }
                    else
                    {
                        seller = new Seller();
                        seller.Email = email;
                        return true;
                    }
                }
            }
            else
            {
                using (var context = new FarmersShopContext())
                {
                    var currentEmail = context.Buyers.Where(a => a.Email.Equals(email)).ToList();
                    if (currentEmail.Count > 0)
                    {
                        return false;
                    }
                    else
                    {
                        buyer = new Buyer();
                        buyer.Email = email;
                        return true;
                    }
                }
            }
        }

        public void SetUserPassword(string password)
        {
            if (user.Type == 1)
            {
                administrator.Password = password;
            }
            else if (user.Type == 2)
            {
                seller.Password = password;
            }
            else
            {
                buyer.Password = password;
            }
        }

        public void SetUserNickname(string nickname)
        {
            if (user.Type == 1)
            {
                administrator.Nickname = nickname;
            }
            else if (user.Type == 2)
            {
                seller.Nickname = nickname;
            }
            else
            {
                buyer.Nickname = nickname;
            }
        }

        public void SetUserAddress(string address)
        {
            if (user.Type == 1)
            {
                administrator.Adress = address;
            }
            else if (user.Type == 2)
            {
                seller.Adress = address;
            }
            else
            {
                buyer.Adress = address;
            }
        }

        public void AddUser()
        {
            using (var context = new FarmersShopContext())
            {
                if (user.Type == 1)
                {
                    user.EntityId = administrator.Id;
                    user.Nickname = administrator.Nickname;
                    user.Email = administrator.Email;
                    context.Administrators.Add(administrator);
                }
                else if (user.Type == 2)
                {
                    user.EntityId = seller.Id;
                    user.Nickname = seller.Nickname;
                    user.Email = seller.Email;
                    context.Sellers.Add(seller);
                }
                else
                {
                    user.EntityId = buyer.Id;
                    user.Nickname = buyer.Nickname;
                    user.Email = buyer.Email;
                    context.Buyers.Add(buyer);
                }
                context.Users.Add(user);
                context.SaveChanges();
            }
            account = new Account();
            account.SetUserType(user.Type);
            account.SetUserEmail(user.Email);
            account.SetUserNicknameAndEntityId(user.Nickname);
        }
    }
}
