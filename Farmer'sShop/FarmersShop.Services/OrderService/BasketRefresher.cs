﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace FarmersShop.Services.OrderService
{
    public class BasketRefresher
    {
        private Buyer buyer;
        public ObservableCollection<Product> products { get; private set; } = new ObservableCollection<Product>();

        public BasketRefresher(Buyer currentBuyer)
        {
            buyer = currentBuyer;
            using (var context = new FarmersShopContext())
            {
                var basketTemp = context.Basket.Where(x => x.Buyer.Id.Equals(buyer.Id)).FirstOrDefault();
                if (basketTemp == null)
                {
                    Basket newBasket = new Basket
                    {
                        Buyer = context.Buyers.Where(x=>x.Id.Equals(currentBuyer.Id)).FirstOrDefault(),
                    };
                    context.Basket.Add(newBasket);
                    context.SaveChanges();
                }
            }
            this.PullBasket();
        }

        public List<Product> GetProducts()
        {
            this.PullBasket();
            var returnList = new List<Product>();
            foreach (var product in products)
            {
                returnList.Add(product);
            }
            return returnList;
        }

        public bool Add(Product product)
        {
                using (var context = new FarmersShopContext())
                {

                    var ifContains = context.BasketProduct.Where(x => x.Product.Id.Equals(product.Id) && x.Basket.Buyer.Id.Equals(buyer.Id) && x.DeletedDate == null).SingleOrDefault();
                    if (ifContains == null)
                    {
                        var newBasketProduct = new BasketProduct
                        {
                            Basket = context.Basket.Where(x => x.Buyer.Id.Equals(buyer.Id)).SingleOrDefault(),
                            Product = context.Products.Where(x => x.Id.Equals(product.Id)).SingleOrDefault()
                        };
                        context.BasketProduct.Add(newBasketProduct);
                        context.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return true;
                    }
                }
            return false;
        }
        public void DeleteFromBasket(Product product)
        {
            if (product != null)
            {
                using (var context = new FarmersShopContext())
                {
                    if (context.BasketProduct.Where(x => x.Product.Id.Equals(product.Id) && x.Basket.Buyer.Id.Equals(buyer.Id) && x.DeletedDate == null).SingleOrDefault() != null)
                        context.BasketProduct.Where(x => x.Product.Id.Equals(product.Id) && x.Basket.Buyer.Id.Equals(buyer.Id) && x.DeletedDate == null).SingleOrDefault().DeletedDate = DateTime.Now;
                    else
                    { }
                    context.SaveChanges();
                }
            }
        }
        public void PullBasket()
        {
            products.Clear();
            using (var context = new FarmersShopContext())
            {
                var temProducts = context.Products.Where(x => x.BasketProducts.Any(x => x.Basket.Buyer.Id.Equals(buyer.Id) && x.DeletedDate == null));
                if (temProducts != null)
                {
                    foreach (var product in temProducts.ToList())
                    {
                        products.Add(product);
                    }
                }
            }
        }
    }
}
