﻿using FarmersShop.ShopDbContext;
using System.Linq;

namespace FarmersShop.Services.RatingsService
{
    public class RatingsUpdater
    {
        public static void UpdateSellersRating()
        {
            using (var context = new FarmersShopContext())
            {
                var allRatings = context.Ratings.ToList();
                var allSellers = context.Sellers.ToList();

                foreach (var seller in allSellers)
                {
                    var sellerRatings = allRatings.Where(rating => rating.ToSeller.Equals(seller)).ToList();
                    var sellerGrade = sellerRatings.Sum(rating => rating.Grade);

                    seller.Rating = sellerGrade / sellerRatings.Count;
                    context.SaveChanges();
                }
            }
        }

        public static void UpdateBuyersRating()
        {
            using (var context = new FarmersShopContext())
            {
                var allRatings = context.Ratings.ToList();
                var allBuyers = context.Buyers.ToList();

                foreach (var buyer in allBuyers)
                {
                    var buyerRatings = allRatings.Where(rating => rating.ToBuyer.Equals(buyer)).ToList();
                    var buyerGrade = buyerRatings.Sum(rating => rating.Grade);

                    buyer.Rating = buyerGrade / buyerRatings.Count;
                    context.SaveChanges();
                }
            }
        }
    }
}
