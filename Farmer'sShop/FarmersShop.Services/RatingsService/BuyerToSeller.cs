﻿using FarmersShop.Domain;
using FarmersShop.ShopDbContext;
using System;
using System.Linq;

namespace FarmersShop.Services.RatingsService
{
    public class BuyerToSeller
    {
        private static Rating rating = new Rating();

        public static void AddGradeAndComment()
        {
            using (var context = new FarmersShopContext())
            {
                int grade;
                Console.Write("Оценка в пятибальной шкале: ");
                Int32.TryParse(Console.ReadLine(), out grade);

                var allBuyers = context.Buyers.ToList();
                var allSellers = context.Sellers.ToList();
                rating.FromBuyer = allBuyers.FirstOrDefault();
                rating.ToSeller = allSellers.FirstOrDefault();
                rating.Grade = grade;

                string comment;
                Console.Write("Комментарий: ");
                comment = Console.ReadLine();

                rating.Comment = comment;
                context.Ratings.Add(rating);
                context.SaveChanges();
            }
            //RatingsUpdater.UpdateSellersRating();
        }
    }
}
