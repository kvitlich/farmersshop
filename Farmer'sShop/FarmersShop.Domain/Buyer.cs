﻿using System.Collections.Generic;

namespace FarmersShop.Domain
{
    public class Buyer : UserElements
    {
        public int Rating { get; set; }
        public virtual BuyerCategory BuyerCategory { get; set; }
        public ICollection<Order> Orders { get; set; } = new List<Order>();
        public ICollection<OtherInfo> OtherInfos { get; set; } = new List<OtherInfo>();
        public ICollection<OtherInfoSubscriptions> OtherInfoSubscriptions { get; set; } = new List<OtherInfoSubscriptions>();
        public ICollection<Chat> Chats { get; set; } = new List<Chat>();

    }
}
