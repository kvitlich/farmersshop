﻿using System.Collections.Generic;

namespace FarmersShop.Domain
{
    public class Chat : Entity
    {
        public virtual Buyer Buyer { get; set; }
        public virtual Seller Seller { get; set; }
        public Administrator Administrator { get; set; }
        public ICollection<ChatMessages> ChatMessages { get; set; } = new List<ChatMessages>();
    }
}
