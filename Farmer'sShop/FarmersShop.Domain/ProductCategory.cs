﻿using System.Collections.Generic;

namespace FarmersShop.Domain
{
    public class ProductCategory : Entity
    {
        public string ProductCategoryName { get; set; }
        public ICollection<Product> Products { get; set; } = new List<Product>();
        public ICollection<BuyersSubscriptions> BuyersSubscriptions { get; set; } = new List<BuyersSubscriptions>();

        public override string ToString()
        {
            return ProductCategoryName;
        }
    }
}
