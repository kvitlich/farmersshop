﻿using System.Collections.Generic;

namespace FarmersShop.Domain
{
    public class Basket : Entity
    {
        public ICollection<BasketProduct> BasketProducts { get; set; } = new List<BasketProduct>();
        public Buyer Buyer { get; set; }
    }
}
