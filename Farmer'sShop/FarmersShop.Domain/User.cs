﻿using System;

namespace FarmersShop.Domain
{
    public class User : Entity
    {
        public Guid EntityId { get; set; }
        public int Type { get; set; } // 1 - Администратор, 2 - Продавец, 3 - Покупатель
        public string Nickname { get; set; }
        public string Email { get; set; }
    }
}