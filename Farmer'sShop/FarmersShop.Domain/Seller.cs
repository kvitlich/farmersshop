﻿using System.Collections.Generic;

namespace FarmersShop.Domain
{
    public class Seller : UserElements
    {
        public int Rating { get; set; }
        public ICollection<Product> Products { get; set; } = new List<Product>();
        public ICollection<OtherInfo> OtherInfos { get; set; } = new List<OtherInfo>();
        public ICollection<OtherInfoSubscriptions> OtherInfoSubscriptions { get; set; } = new List<OtherInfoSubscriptions>();
        public ICollection<Chat> Chats { get; set; } = new List<Chat>();
    }
}
