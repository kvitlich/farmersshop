﻿namespace FarmersShop.Domain
{
    public class UserElements : Entity
    {
        public string Nickname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Adress { get; set; }
    }
}