﻿namespace FarmersShop.Domain
{
    public class OtherInfoSubscriptions : Entity
    {
        public virtual OtherInfo OtherInfo { get; set; }
        public virtual Buyer SubscriberBuyer { get; set; }
        public virtual Seller SubscriberSeller { get; set; }
    }
}
