﻿namespace FarmersShop.Domain
{
    public class ChatMessages : Entity
    {
        public string Text { get; set; }
        public virtual Chat Chat { get; set; }
        public virtual User FromUser { get; set; }
        public virtual OtherInfo OtherInfo { get; set; }
    }
}
