﻿namespace FarmersShop.Domain
{
    public class BasketProduct : Entity
    {
        public virtual Basket Basket { get; set; }
        public virtual Product Product { get; set; }
    }
}
