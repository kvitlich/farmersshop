﻿using System;

namespace FarmersShop.Domain
{
    public class Order : Entity
    {
        public virtual Buyer Buyer { get; set; }
        public virtual Product Product { get; set; }
        public int Kgs { get; set; }
        public DateTime? CheckedDate { get; set; }
        public virtual Administrator Administrator { get; set; }
    }
}
