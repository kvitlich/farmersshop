﻿using System.Collections.Generic;

namespace FarmersShop.Domain
{
    public class BuyerCategory : Entity
    {
        public string BuyerCategoryName { get; set; }
        public ICollection<Buyer> Buyers { get; set; } = new List<Buyer>();
    }
}
