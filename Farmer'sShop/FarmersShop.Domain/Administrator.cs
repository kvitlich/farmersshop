﻿using System.Collections.Generic;

namespace FarmersShop.Domain
{
    public class Administrator : UserElements
    {
        public ICollection<Order> Orders { get; set; } = new List<Order>();
    }
}
